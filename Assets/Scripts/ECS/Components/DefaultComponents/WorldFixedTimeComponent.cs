﻿namespace ECS
{
    [System.Serializable]
    public struct WorldFixedTimeComponent : IComponent
    {
        public float FixedTime;
        public int FixedTickCount;

        void IData.WriteToDataLayout(in int world, in int entity) =>
            Core.ComponentMap<WorldFixedTimeComponent>.AddOrSet(world, entity, this);
    }
}
