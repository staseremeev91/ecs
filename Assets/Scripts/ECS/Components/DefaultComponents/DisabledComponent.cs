namespace ECS
{
    [System.Serializable]
    public struct DisabledComponent : IComponent
    {
        void IData.WriteToDataLayout(in int world, in int entity) =>
            Core.ComponentMap<DisabledComponent>.AddOrSet(world, entity, default);
    }
}
