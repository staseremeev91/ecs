﻿using UnityEngine;

namespace ECS
{
    [System.Serializable, UnityProxy.ProxyComponent(UnityProxy.ProxyComponenentState.Persistent)]
    public struct GameObjectProxyComponenet : IComponent
    {
        public string Name;
        public string Tag;
        public int Layer;
        public bool IsActive;
        
        void IData.WriteToDataLayout(in int world, in int entity) =>
            Core.ComponentMap<GameObjectProxyComponenet>.AddOrSet(world, entity, this);

        public static GameObjectProxyComponenet New(in GameObject gameObject) =>
            new()
            {
                Name = gameObject.name,
                Tag = gameObject.tag,
                Layer = gameObject.layer,
                IsActive = gameObject.activeSelf,
            };
    }
}
