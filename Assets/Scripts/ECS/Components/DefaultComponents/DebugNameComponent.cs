﻿namespace ECS
{
    [System.Serializable, UnityProxy.ProxyComponent(UnityProxy.ProxyComponenentState.Persistent)]
    public struct DebugNameComponent : IComponent
    {
        [UnityEngine.SerializeField]
        string m_DebugName;

        public string DebugName => m_DebugName;

        void IData.WriteToDataLayout(in int world, in int entity) =>
            Core.ComponentMap<DebugNameComponent>.AddOrSet(world, entity, this);

        public static DebugNameComponent New(in string name) =>
            new() { m_DebugName = name };
    }
}
