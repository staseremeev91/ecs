﻿namespace ECS
{
    [System.Serializable]
    public struct WorldTimeComponent : IComponent
    {
        public float Time;
        public int TickCount;

        void IData.WriteToDataLayout(in int world, in int entity) =>
            Core.ComponentMap<WorldTimeComponent>.AddOrSet(world, entity, this);
    }
}
