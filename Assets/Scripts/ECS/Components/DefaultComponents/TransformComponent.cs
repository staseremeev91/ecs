﻿using UnityEngine;

namespace ECS
{
    [System.Serializable, UnityProxy.ProxyComponent(UnityProxy.ProxyComponenentState.Persistent)]
    public struct TransformComponent : IComponent
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        void IData.WriteToDataLayout(in int world, in int entity) =>
            Core.ComponentMap<TransformComponent>.AddOrSet(world, entity, this);

        public static TransformComponent New(in Transform transform) =>
            new()
            {
                Position = transform.position,
                Rotation = transform.rotation,
                Scale = transform.localScale,
            };
    }
}
