using System.Collections.Generic;
using ECS;
using ECS.Core;
using UnityEditor;
using UnityEngine;

public class EntityDebugger : EditorWindow
{
    Vector2 m_EntityListScrollPosition;
    Vector2 m_ComponentListScrollPosition;
    Vector2 m_SystemListScrollPosition;

    int m_CurrentInspectedEntity = int.MinValue;
    IEntityQuery m_CurrentQuery;

    [MenuItem("Window/My Window")]
    static void Init()
    {
        GetWindow<EntityDebugger>().Show();
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField($"Components type count: {DataTypeUtility.Types.Count}");

        if (!EditorApplication.isPlaying)
        {
            EditorGUILayout.HelpBox("Entity inspection is only awaliable in play mode", MessageType.Warning);
            m_CurrentInspectedEntity = int.MinValue;
            return;
        }

        if (m_CurrentQuery == null)
        {
            m_CurrentQuery = EntityQueryBuilder.NewQueryForAllEntities(includeDisabled: true);
            m_CurrentQuery.ForceRebuild();
        }

        EditorGUILayout.BeginVertical();
        foreach (var world in ECSWorld.All)
            DrawWorld(world);

        EditorGUILayout.EndVertical();
    }

    void DrawWorld(in ECSWorld world)
    {
        EditorGUILayout.LabelField($"World: {world.UID} Valid: {world.IsValid} Entities: {world.EntitiesCount}");

        EditorGUI.indentLevel++;
        {
            try
            {
                var entityNameComponents = world.GetComponentsWithEntityReadOnly<DebugNameComponent>();

                EditorGUILayout.BeginHorizontal();
                {
                    // Draw entity list.
                    var entityButtonSize = 120;
                    m_EntityListScrollPosition = EditorGUILayout.BeginScrollView(m_EntityListScrollPosition, GUILayout.Width(entityButtonSize + 20));
                    EditorGUILayout.BeginVertical();
                    {
                        foreach (var entity in m_CurrentQuery.GetEntities(world.UID))
                        {
                            var entityName = GetEntityName(entityNameComponents, entity);

                            if (GUILayout.Button(entityName, GUILayout.Width(entityButtonSize)))
                                m_CurrentInspectedEntity = entity;
                        }
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndScrollView();

                    // Draw component list for entity.
                    m_ComponentListScrollPosition = EditorGUILayout.BeginScrollView(m_ComponentListScrollPosition, GUILayout.Width(350));
                    EditorGUILayout.BeginVertical();
                    {
                        var entityExists = SharedComponentMap_EditorProxy.TryGetDataTypeHashes(world.UID, m_CurrentInspectedEntity, out var components, out var totalSize);
                        EditorGUILayout.LabelField(entityExists ? $"{GetEntityName(entityNameComponents, m_CurrentInspectedEntity)} [{totalSize} bytes]:" : "Select entity to inspect");

                        if (entityExists)
                        {
                            foreach (var componentTypeHash in components)
                                DrawComponent(world.UID, componentTypeHash);
                        }
                    }                    
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndScrollView();

                    // Draw component list for entity.
                    m_SystemListScrollPosition = EditorGUILayout.BeginScrollView(m_SystemListScrollPosition, GUILayout.Width(500));
                    EditorGUILayout.BeginVertical();
                    {
                        EditorGUI.indentLevel++;
                        DrawSystem(world, world.RootSystem);
                        EditorGUI.indentLevel--;
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndScrollView();

                }
                EditorGUILayout.EndHorizontal();
            }
            catch (System.Exception e)
            {
                EditorGUILayout.LabelField(e.Message);
            }
        }
        EditorGUI.indentLevel--;
    }

    private static void DrawSystem(in ECSWorld world, in ISystemBase system)
    {
        if (system is ISystem executionSystem)
        {
            var titile = $"{system.GetType().FullName}";
            if (executionSystem.TrackedQueries.Count > 0)
                titile += $": query: {executionSystem.TrackedQueries.Count}";

            titile += $" [{system.LastUpdateTimeMS_Editor:f3}ms]";

            EditorGUILayout.BeginHorizontal();
            //system.Enabled = EditorGUILayout.Toggle(system.Enabled, GUILayout.Width(50));
            EditorGUILayout.LabelField(titile);
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel++;

            var queryNumber = 1;
            foreach (var query in executionSystem.TrackedQueries)
            {
                DrawQuery(query, queryNumber.ToString());
                queryNumber++;
            }
            EditorGUI.indentLevel--;
        }

        if (system is ISystemGroup groupSystem)
        {
            if (groupSystem.Systems.Count > 0)
            {
                foreach (var childSysem in groupSystem.Systems)
                {
                    EditorGUI.indentLevel++;
                    DrawSystem(world, childSysem);
                    EditorGUI.indentLevel--;
                }
            }
        }
    }

    private static void DrawQuery(in ISystemEntityQuery query, in string name)
    {
        EditorGUILayout.LabelField($"Query #{name} Entities: {query.Count()}");
        TryDrawQueryComponents("All", query.All, query);
        TryDrawQueryComponents("Any", query.Any, query);
        TryDrawQueryComponents("None", query.Exclude, query);
        TryDrawQueryComponents("Optional", query.Optional, query);
    }

    private static void TryDrawQueryComponents(in string title, in IReadOnlyCollection<int> components, in ISystemEntityQuery query)
    {
        if (components.Count > 0)
        {
            EditorGUILayout.LabelField($"{title}: {components.Count}");
            EditorGUI.indentLevel++;
            foreach (var componentTypeHash in components)
            {
                DataTypeUtility.TryGetType(componentTypeHash, out var type);
                var postfix = GetComponentOptionsMask(componentTypeHash, query);
                EditorGUILayout.LabelField($"{type.FullName} {postfix}");
            }

            EditorGUI.indentLevel--;
        }
    }

    private static string GetComponentOptionsMask(in int componentTypeHash, in ISystemEntityQuery query)
    {
        var result = "[";

        var optionsRecorded = 0;
        foreach (QueryComponentOptions option in System.Enum.GetValues(typeof(QueryComponentOptions)))
        {
            if (!query.IsOptionEnabled(componentTypeHash, option))
                continue;

            result += optionsRecorded == 0 ? option.ToString() : $" & {option}";
            optionsRecorded++;
        }

        return $"{result}]";
    }

    private static string GetEntityName(in ReadOnlyComponentAccessor<DebugNameComponent> entityNames, in int entity) =>
        entityNames.TryGet(entity, out var debugNameComponent) ?
        debugNameComponent.DebugName :
        $"Entity: {entity}";

    void DrawComponent(in int world, in int componentTypeHash)
    {
        SharedComponentMap_EditorProxy.TryGetDataByHash(world, m_CurrentInspectedEntity, componentTypeHash, out var component);
        var sizeBytes = DataTypeUtility.SizeOf(componentTypeHash);

        var componentType = component.GetType();
        EditorGUILayout.LabelField($"{componentType.FullName} [{sizeBytes} bytes]:");
        DrawNestedField(componentType, component);
    }

    void DrawNestedField(System.Type type, object objectToDraw, int currenDepth = 0)
    {
        if (currenDepth == 8)
            return;

        currenDepth++;

        EditorGUI.indentLevel++;
        {
            var fields = GetFieldsToDraw(type);
            foreach (var filed in fields)
            {
                if (filed.IsPublic || System.Attribute.IsDefined(filed, typeof(SerializeField)))
                {
                    var currentObject = filed.GetValue(objectToDraw);
                    if (currentObject is System.Collections.IList collection)
                    {
                        EditorGUILayout.LabelField($"{filed.Name}: {collection.Count}");
                        foreach (var e in collection)
                            DrawNestedField(e.GetType(), e, currenDepth);
                    }
                    else
                    {
                        EditorGUILayout.LabelField($"{filed.Name}: {filed.GetValue(objectToDraw)}");
                        DrawNestedField(filed.FieldType, currentObject, currenDepth);
                    }
                }
            }
        }
        EditorGUI.indentLevel--;
    }

    System.Reflection.FieldInfo[] GetFieldsToDraw(System.Type type) =>
        type.GetFields(
            System.Reflection.BindingFlags.Public |
            System.Reflection.BindingFlags.NonPublic |
            System.Reflection.BindingFlags.Instance |
            System.Reflection.BindingFlags.DeclaredOnly |
            System.Reflection.BindingFlags.GetField);

}
