﻿using System.Collections;
using System.Collections.Generic;

public interface IData
{
    void WriteToDataLayout(in int world, in int entity);
}

public interface IComponent : IData
{
    
}


public interface IBufferComponent : IData
{

}

public struct Buffer<TBufferElement> : IEnumerable<TBufferElement>, IBufferComponent where TBufferElement : struct, IBufferElement
{
    public int Count => m_InternalBuffer.Count;
    public int Capacity => m_InternalBuffer.Capacity;

    [UnityEngine.SerializeField]
    List<TBufferElement> m_InternalBuffer;
    bool m_WasWrittenToLayout;

    public static Buffer<TBufferElement> Allocate(in int initialCapacity = 32) =>
        new()
        {
            m_InternalBuffer = new List<TBufferElement>(initialCapacity),
            m_WasWrittenToLayout = false
        };


    public static Buffer<TBufferElement> AllocateTo(in int world, in int entity, in int initialCapacity = 32)
    {
        var neBuffer = Allocate(initialCapacity);
        ECS.Core.BufferMap<TBufferElement>.AddOrSet(world, entity, neBuffer);
        return neBuffer;
    }

    public TBufferElement this[int index]
    {
        get => m_InternalBuffer[index];
        set => m_InternalBuffer[index] = value;
    }

    public void Add(in TBufferElement element) =>
        m_InternalBuffer.Add(element);

    public void AddRange(IEnumerable<TBufferElement> other) =>
        m_InternalBuffer.AddRange(other);

    public void AddRange(Buffer<TBufferElement> other) =>
        m_InternalBuffer.AddRange(other);

    public void Clear() =>
        m_InternalBuffer.Clear();

    public TBufferElement[] ToArray() =>
        m_InternalBuffer.ToArray();

    public ReadOnly AsReadOnly() =>
        new (this);

    public void WriteToDataLayout(in int world, in int entity)
    {
        var bufferToWrite = this;

        // Special case for buffers. If this buffer was ever written to layout, the we consider to making the copy every time.
        if (m_WasWrittenToLayout)
        {
            var neBuffer = Allocate();
            if (m_InternalBuffer != null)
                neBuffer.m_InternalBuffer.AddRange(m_InternalBuffer);
        }

        ECS.Core.BufferMap<TBufferElement>.AddOrSet(world, entity, bufferToWrite);
        m_WasWrittenToLayout = true;
    }

    public IEnumerator<TBufferElement> GetEnumerator() =>
        m_InternalBuffer.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() =>
        m_InternalBuffer.GetEnumerator();

    public struct ReadOnly : IEnumerable<TBufferElement>
    {
        public int Count => m_TargetBuffer.Count;
        public int Capacity => m_TargetBuffer.Capacity;

        Buffer<TBufferElement> m_TargetBuffer;

        public IBufferElement this[int index] =>
            m_TargetBuffer[index];

        public ReadOnly(in Buffer<TBufferElement> buffer) =>
            m_TargetBuffer = buffer;

        public TBufferElement[] ToArray() =>
            m_TargetBuffer.ToArray();

        public IEnumerator<TBufferElement> GetEnumerator() =>
            m_TargetBuffer.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() =>
            m_TargetBuffer.GetEnumerator();
    }
}

public interface IBufferElement
{

}

public struct LinkedEntityBufferElement : IBufferElement
{
    public ECS.Core.Ptr.EntityPtr Entity;

    public static Buffer<LinkedEntityBufferElement> Allocate(in int initialCapacity = 32) =>
        Buffer<LinkedEntityBufferElement>.Allocate(initialCapacity);
}

/// TODO: Binary serialization;
public interface IBinarySerializedCompoent
{
    void Serialize(System.IO.BinaryWriter writer);
    void Deserialize(System.IO.BinaryReader writer);
}