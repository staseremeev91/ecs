﻿using System;
using System.Collections.Generic;

namespace ECS.UnityProxy
{
    public static class ProxyComponentUtility
    {
        readonly static HashSet<int> s_AllProxyComponenents = new();
        readonly static HashSet<int> s_PersistentComponenentTypes = new();

        static ProxyComponentUtility()
        {
            foreach (var type in Core.DataTypeUtility.GetComponenentTypes())
            {
                if (type.IsGenericType)
                    continue;

                var typeHashCode = Core.DataTypeUtility.HashCodeOf(type);

                if (Attribute.GetCustomAttribute(type, typeof(ProxyComponentAttribute)) is ProxyComponentAttribute attribute)
                {
                    s_AllProxyComponenents.Add(typeHashCode);
                    if (attribute.State == ProxyComponenentState.Persistent)
                        s_PersistentComponenentTypes.Add(typeHashCode);
                }
            }
        }

        public static IReadOnlyCollection<int> GetComponenentTypeHashCodes() => s_AllProxyComponenents;
        public static IReadOnlyCollection<int> GetPersistentComponenentTypeHashCodes() => s_PersistentComponenentTypes;

        public static bool IsPersistentProxyComponent(this Type type) =>
            s_PersistentComponenentTypes.Contains(Core.DataTypeUtility.HashCodeOf(type));
    }
}