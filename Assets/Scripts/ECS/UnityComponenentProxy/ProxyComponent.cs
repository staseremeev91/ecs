﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ECS.UnityProxy
{
    public struct TestBufferComponent : IBufferElement
    {
        public int IntValue;
        public Vector3 Vector3;
    }


    [DisallowMultipleComponent]
    public sealed class ProxyComponent : MonoBehaviour
    {
        [SerializeField]
        bool m_ConvertOnStart;

        [SerializeReference]
        List<IComponent> m_Components = new();

        [SerializeField, Multiline]
        string m_Serialized;

        void Start()
        {
            if (m_ConvertOnStart)
                ToBuilder(world: 1).BuildNow();
        }

        [ContextMenu("Serialize")]
        void Serialize()
        {
            var world = int.MaxValue;
            var builder = ToBuilder(world);
            builder.BuildNow();
            var snapshot = new WorldSnapshot(world);
            m_Serialized = snapshot.ToJSON();
            GUIUtility.systemCopyBuffer = m_Serialized;

            Core.SharedComponentMap.RemoveDataForWorld(world);
        }

        void OnValidate()
        {
            foreach(var componenentHash in ProxyComponentUtility.GetPersistentComponenentTypeHashCodes())
            {
                if (!Core.DataTypeUtility.TryGetType(componenentHash, out var componenentType))
                    continue;

                if (!CanAddComponent(componenentType))
                    continue;

                //m_Components.Insert(0, Core.SharedComponentMap.ProduseComponent(componenentType));
            }
        }

        public bool TryAddComponenent(Type componenentType)
        {
            var canAddComponentnt = CanAddComponent(componenentType);
            //if(canAddComponentnt)
            //    m_Components.Add(Core.SharedComponentMap.ProduseComponent(componenentType));

            return canAddComponentnt;
        }

        public bool CanAddComponent(Type componenentType)
        {
            foreach (var component in m_Components)
                if (component.GetType().Equals(componenentType))
                    return false;

            return true;
        }

        public bool TryAddComponenent(IComponent componenent)
        {
            var componenentType = componenent.GetType();
            var canAddComponentnt = CanAddComponent(componenentType);
            if (canAddComponentnt)
                m_Components.Add(componenent);

            return canAddComponentnt;
        }

        public EntityLazyBuilder ToBuilder(in int world)
        {
            var builder = EntityLazyBuilder
                .New(world)
                .AddComponents(m_Components);

            builder.AddLinkedEntityBuffer();
            return builder;
        }
       

        public IReadOnlyList<IComponent> Components => m_Components;

        public void SetTransform(TransformComponent newTransformComponent)
        {
            for(var i = 0; i < m_Components.Count; i++)
                if (m_Components[i] is TransformComponent)
                    m_Components[i] = newTransformComponent;
        }

        public void ConvertCurrentGameObject()
        {
            var components = GetComponents<IProxyComponenentConverter>();
            foreach (var component in components)
                TryAddComponenent(component.Convert());
        }
    }    
}

