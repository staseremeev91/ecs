﻿using System;

namespace ECS.UnityProxy
{
    public enum ProxyComponenentState
    {
        Default,
        Persistent,
    }

    [AttributeUsage(AttributeTargets.Struct, AllowMultiple = false, Inherited = false)]
    public class ProxyComponentAttribute : Attribute
    {
        public readonly ProxyComponenentState State;

        public ProxyComponentAttribute() : this(ProxyComponenentState.Default) { }
        public ProxyComponentAttribute(ProxyComponenentState state) => State = state;
    }
}
