﻿using ECS.Collections;
using ECS.Core;
using ECS.Core.Ptr;
using System;
using System.Collections.Generic;

namespace ECS
{
    /// <summary>
    /// Allocates new entity;
    /// </summary>
    public struct EntityBuilder
    {
        readonly int m_WorldUID;
        readonly int m_EntityUID;

        EntityBuilder(in int world)
        {
            m_WorldUID = world;
            m_EntityUID = EntityAllocatorComponenent.Allocate(m_WorldUID);
        }

        public void AddComponenet<TComponent>(TComponent component) where TComponent : struct, IComponent =>
            ComponentMap<TComponent>.TryAdd(m_WorldUID, m_EntityUID, component);

        public void AddBuffer<TBufferElement>(Buffer<TBufferElement> buffer) where TBufferElement : struct, IBufferElement =>
            BufferMap<TBufferElement>.TryAdd(m_WorldUID, m_EntityUID, buffer);

        public void SetDebugName(string name) =>
            AddComponenet(DebugNameComponent.New(name));

        public void AddLinkedEntityBuffer() =>
            BufferMap<LinkedEntityBufferElement>.TryAdd(m_WorldUID, m_EntityUID, LinkedEntityBufferElement.Allocate());

        public static EntityBuilder New(in int world) => new(world);

        public EntityPtr ToEntityPtr() =>
            new(m_WorldUID, m_EntityUID);
    }

    /// <summary>
    /// Allocates new entity;
    /// </summary>
    public struct EntityLazyBuilder : IDisposable
    {
        readonly int m_WorldUID;
        readonly int m_EntityUID;

        PooledList<IData> m_Data;

        bool IsValid => m_Data != null;

        EntityLazyBuilder(in int world)
        {
            m_WorldUID = world;
            m_EntityUID = EntityAllocatorComponenent.Allocate(m_WorldUID);
            m_Data = PooledList<IData>.Provide();
        }

        public EntityLazyBuilder AddComponents(in IEnumerable<IComponent> components)
        {
            if (!IsValid)
                throw new InvalidOperationException($"Entity '{m_EntityUID}' for world '{m_WorldUID}' is invalid");

            m_Data.AddRange(components);
            return this;
        }

        public EntityLazyBuilder AddComponent(in IComponent component)
        {
            if (!IsValid)
                throw new InvalidOperationException($"Entity '{m_EntityUID}' for world '{m_WorldUID}' is invalid");

            m_Data.Add(component);
            return this;
        }

        public EntityLazyBuilder AddBuffer(in IBufferComponent component)
        {
            if (!IsValid)
                throw new InvalidOperationException($"Entity '{m_EntityUID}' for world '{m_WorldUID}' is invalid");

            m_Data.Add(component);
            return this;
        }

        public EntityLazyBuilder AddLinkedEntityBuffer()
        {
            if (!IsValid)
                throw new InvalidOperationException($"Entity '{m_EntityUID}' for world '{m_WorldUID}' is invalid");

            m_Data.Add(LinkedEntityBufferElement.Allocate());
            return this;
        }

        public EntityLazyBuilder SetDebugName(in string name)
        {
            AddComponent(DebugNameComponent.New(name));
            return this;
        }

        public EntityPtr BuildNow()
        {
            if (!IsValid)
                throw new InvalidOperationException($"Entity '{m_EntityUID}' for world '{m_WorldUID}' is invalid");

            if (m_Data.Count == 0)
                throw new InvalidOperationException($"Unable to build entity with zero components");

            foreach (var c in m_Data)
                c.WriteToDataLayout(m_WorldUID, m_EntityUID);

            Dispose();

            return new(m_WorldUID, m_EntityUID);
        }

        public void BuildViaCommandBuffer(in int world, in int entity, ICommandBufferConcatenator commndBuffer)
        {
            if (!IsValid)
                throw new InvalidOperationException($"Entity '{m_EntityUID}' for world '{m_WorldUID}' is invalid");

            foreach (var component in m_Data)
                commndBuffer.TryAddOrSetData(world, entity, component);

            Dispose();
        }

        public EntityPtr ToEntityPtr() => new(m_WorldUID, m_EntityUID);

        public void Dispose()
        {
            m_Data.Free();
            m_Data = null;
        }

        public static EntityLazyBuilder New(in int world) => new(world);

        public static EntityLazyBuilder operator +(EntityLazyBuilder builder, IComponent component)
        {
            if (!builder.IsValid)
                throw new InvalidOperationException($"Invalid builder");

            return builder.AddComponent(component);
        }

        public static EntityLazyBuilder operator +(EntityLazyBuilder builder, IBufferComponent buffer)
        {
            if (!builder.IsValid)
                throw new InvalidOperationException($"Invalid builder");

            return builder.AddBuffer(buffer);
        }
    }
}
