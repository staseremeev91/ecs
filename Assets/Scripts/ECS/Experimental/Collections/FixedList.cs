﻿using UnityEngine;

namespace ECS.Core.Experimental.Collections
{
    public interface IFixedList<TElement> where TElement : struct
    {
        int Length { get; }
        TElement this[in int index] { get; set; }
        void Add(in TElement value);
        void Clear();
        TElement[] ToArray();
    }

    [System.Serializable]
    public struct FixedList<TArray, TElement> : IFixedList<TElement> where TArray : struct, IFixedArray<TElement> where TElement : struct
    {
        [SerializeField]
        TArray m_InternalArray;

        [SerializeField]
        int m_Position;
        public int Length => m_Position;

        public TElement this[in int index]
        {
            get => Get(index);
            set => Set(index, value);
        }

        public void Add(in TElement value)
        {
            if (m_Position >= m_InternalArray.Capacity)
                throw new System.OverflowException();

            m_InternalArray[m_Position] = value;
            m_Position++;
        }

        public void Clear() =>
            m_Position = 0;

        TElement Get(in int index)
        {
            ValidateIndex(index);
            return m_InternalArray[index];
        }

        void Set(in int index, in TElement value)
        {
            ValidateIndex(index);
            m_InternalArray[index] = value;
        }

        void ValidateIndex(in int index)
        {
            if (index < 0 || index >= m_Position)
                throw new System.IndexOutOfRangeException();
        }

        public TElement[] ToArray()
        {
            var result = new TElement[Length];
            for (var i = 0; i < result.Length; i++)
                result[i] = m_InternalArray[i];

            return result;
        }
    }

    [System.Serializable]
    public struct FixedList8<TElement> : IFixedList<TElement> where TElement : struct
    {
        [SerializeField]
        FixedList<FixedArray8<TElement>, TElement> m_InternalList;

        public TElement this[in int index]
        {
            get => m_InternalList[index];
            set => m_InternalList[index] = value;
        }

        public int Length =>
            m_InternalList.Length;

        public void Add(in TElement value) =>
            m_InternalList.Add(value);

        public void Clear() =>
            m_InternalList.Clear();

        public TElement[] ToArray() =>
            m_InternalList.ToArray();
    }

    [System.Serializable]
    public struct FixedList16<TElement> : IFixedList<TElement> where TElement : struct
    {
        [SerializeField]
        FixedList<FixedArray16<TElement>, TElement> m_InternalList;

        public TElement this[in int index]
        {
            get => m_InternalList[index];
            set => m_InternalList[index] = value;
        }

        public int Length =>
            m_InternalList.Length;

        public void Add(in TElement value) =>
            m_InternalList.Add(value);

        public void Clear() =>
            m_InternalList.Clear();

        public TElement[] ToArray() =>
            m_InternalList.ToArray();
    }

    [System.Serializable]
    public struct FixedList32<TElement> : IFixedList<TElement> where TElement : struct
    {
        [SerializeField]
        FixedList<FixedArray32<TElement>, TElement> m_InternalList;

        public TElement this[in int index]
        {
            get => m_InternalList[index];
            set => m_InternalList[index] = value;
        }

        public int Length =>
            m_InternalList.Length;

        public void Add(in TElement value) =>
            m_InternalList.Add(value);

        public void Clear() =>
            m_InternalList.Clear();

        public TElement[] ToArray() =>
            m_InternalList.ToArray();
    }

    [System.Serializable]
    public struct FixedList64<TElement> : IFixedList<TElement> where TElement : struct
    {
        [SerializeField]
        FixedList<FixedArray64<TElement>, TElement> m_InternalList;

        public TElement this[in int index]
        {
            get => m_InternalList[index];
            set => m_InternalList[index] = value;
        }

        public int Length =>
            m_InternalList.Length;

        public void Add(in TElement value) =>
            m_InternalList.Add(value);

        public void Clear() =>
            m_InternalList.Clear();

        public TElement[] ToArray() =>
            m_InternalList.ToArray();
    }
}
