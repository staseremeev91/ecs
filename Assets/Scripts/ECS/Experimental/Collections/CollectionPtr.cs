﻿using ECS.Core.Ptr;
using UnityEngine;

namespace ECS.Core.Experimental.Collections
{
    [System.Serializable]
    public struct CollectionLayoutPointerComponent : IComponent
    {
        [SerializeField]
        EntityRangePtr m_Layout;

        public EntityRangePtr Layout => m_Layout;

        public void UpdateLayout(in EntityRangePtr layout) =>
            m_Layout = layout;

        public void WriteToDataLayout(in int world, in int entity) =>
            ComponentMap<CollectionLayoutPointerComponent>.AddOrSet(world, entity, this);
    }

    [System.Serializable]
    public struct CollectionPtr<TElement> : System.IDisposable where TElement : unmanaged
    {
        public delegate void ForEachReadWriteProcessorDelegate(ref TElement element, in int index);
        public delegate void ForEachReadOnlyProcessorDelegate(TElement element, in int index);

        [SerializeField]
        ComponentPtr<CollectionLayoutPointerComponent> m_Pointer;
        [SerializeField]
        CollectionPtrSlice<TElement> m_Slice;

        CollectionPtr(in EntityPtr pointer)
        {
            m_Pointer = pointer.ToComponentPtr<CollectionLayoutPointerComponent>();
            m_Slice = CollectionPtrSlice<TElement>.New(entityRange: m_Pointer.Get().Layout);
        }

        public bool IsValid =>
            m_Pointer.IsValid;

        public int Length =>
            m_Slice.Length;

        public TElement this[in int index]
        {
            get => Get(index);
            set => Set(index, value);
        }

        TElement Get(in int index) =>
            m_Slice[index];

        void Set(in int index, in TElement value) =>
            m_Slice[index] = value;

        public TElement[] ToArray() =>
            m_Slice.ToArray();

        public void Clear() =>
            m_Slice.Clear();

        public void CopyTo(ref CollectionPtr<TElement> other, in int offset = 0) =>
            m_Slice.CopyTo(ref other, offset);

        public CollectionPtr<TElement> CloneWithSize(in int length) =>
            m_Slice.CloneWithSize(length);

        public CollectionPtr<TElement> CloneWith(in CollectionPtr<TElement> other)
        {
            var newCollection = CloneWithSize(Length + other.Length);
            other.CopyTo(ref newCollection, Length);
            return newCollection;
        }

        public CollectionPtrSlice<TElement> GetSlice(in int position, in int length) =>
            m_Slice.GetSlice(position, length);

        public void Resize<TCommandBuffer>(in int length, in TCommandBuffer commandBuffer) where TCommandBuffer : ICommandBufferCommonModificator, ICommandBufferConcatenator
        {
            var allocationWorld = m_Pointer.EntityPtr.World;
            ref var allocator = ref EntityAllocatorComponenent.Get(allocationWorld);

            // Store the pointer to entity range before resize, to be able to dispose it later on.
            var rangeToDispose = m_Pointer.Get().Layout;

            // Allocate and assign new range.
            var newCollectionRange = AllocateCollectionRange(allocationWorld, length, commandBuffer, ref allocator);

            // Update collection layout.
            m_Pointer.GetByRef().UpdateLayout(newCollectionRange);

            // Update internal slice.
            m_Slice = CollectionPtrSlice<TElement>.New(newCollectionRange);

            // Copy elements to new entity range.
            m_Slice.CopyFrom(CollectionPtrSlice<TElement>.New(rangeToDispose));

            // Dispose old range.
            commandBuffer.Schedule(new DisposeCommand<EntityRangePtr>(rangeToDispose));
        }

        public void Dispose()
        {
            m_Slice = default;

            m_Pointer.Get().Layout.Dispose();
            m_Pointer.Dispose();
            m_Pointer = default;
        }

        public override string ToString() =>
            $"{typeof(TElement).FullName}: {m_Slice}";

        public static implicit operator EntityPtr(CollectionPtr<TElement> collection) => 
            collection.m_Pointer.EntityPtr;

        public static CollectionPtr<TElement> New(in EntityPtr pointer) => 
            new(pointer);

        public static CollectionPtr<TElement> Allocate(in int world, in int length) =>
            AllocateViaCommandBuffer(world, length, new DirectCommandBufferConcatenator());

        public static CollectionPtr<TElement> AllocateViaCommandBuffer<TCommandBuffer>(in int world, in int length, in TCommandBuffer commandBuffer) where TCommandBuffer : ICommandBufferConcatenator
        {
            if (length <= 0)
                throw new System.IndexOutOfRangeException();

            ref var allocator = ref EntityAllocatorComponenent.Get(world);
            var collectionRange = AllocateCollectionRange(world, length, commandBuffer, ref allocator);
            var mainCollectionPointer = AllocateCollectionPointer(world, commandBuffer, collectionRange, ref allocator);

            return New(mainCollectionPointer);
        }

        static EntityPtr AllocateCollectionPointer<TCommandBuffer>(in int world, in TCommandBuffer commandBuffer, EntityRangePtr collectionRange, ref EntityAllocatorComponenent allocator) where TCommandBuffer : ICommandBufferConcatenator
        {
            var mainCollectionPointer = new EntityPtr(world, allocator.AllocateUID);

            var mainCollectionComponent = default(CollectionLayoutPointerComponent);
            mainCollectionComponent.UpdateLayout(collectionRange);

            commandBuffer.TryAddOrSetComponent(world, mainCollectionPointer.Entity, mainCollectionComponent);
            return mainCollectionPointer;
        }

        static EntityRangePtr AllocateCollectionRange<TCommandBuffer>(in int world, in int length, in TCommandBuffer commandBuffer, ref EntityAllocatorComponenent allocator) where TCommandBuffer : ICommandBufferConcatenator
        {
            var rangeValidator = new EntityPtr(world, allocator.AllocateUID);
            commandBuffer.TryAddOrSetComponent(world, rangeValidator.Entity, new EntityPtrValidatorComponent());

            var entityRangePtr = new EntityRangePtr(rangeValidator, allocator.NextEntityUID, length);
            for (var i = 0; i < length; i++)
            {
                var entityUID = allocator.AllocateUID;
                commandBuffer.TryAddOrSetComponent(world, entityUID, new CollectionElementTagComponent());
                commandBuffer.TryAddOrSetComponent(world, entityUID, new CollectionElementComponent<TElement>(rangeValidator));
            }

            return entityRangePtr;
        }
    }
}
