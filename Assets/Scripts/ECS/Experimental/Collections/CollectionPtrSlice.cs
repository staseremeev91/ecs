using ECS.Core.Ptr;

namespace ECS.Core.Experimental.Collections
{
    [System.Serializable]
    public struct CollectionPtrSlice<TElement> where TElement : unmanaged
    {
        [UnityEngine.SerializeField]
        EntityRangePtr m_Elements;

        CollectionPtrSlice(in EntityRangePtr elements) =>
            m_Elements = elements;

        public bool IsValid =>
            m_Elements.IsValid;

        public int Length =>
            m_Elements.Length;

        public TElement this[in int index]
        {
            get => Get(index);
            set => Set(index, value);
        }

        TElement Get(in int index)
        {
            var targetIndex = ValidateOperation(index);
            if (!ComponentMap<CollectionElementComponent<TElement>>.TryGet(m_Elements.World, targetIndex, out var component))
                throw new System.InvalidOperationException();

            if (!component.HasValue)
                throw new System.InvalidOperationException();

            return component.Value;
        }

        void Set(in int index, in TElement value)
        {
            var targetIndex = ValidateOperation(index);
            ComponentMap<CollectionElementComponent<TElement>>.AddOrSet(m_Elements.World, targetIndex, new CollectionElementComponent<TElement>(m_Elements.Validator, value));
        }

        int ValidateOperation(in int index)
        {
            if (!IsValid)
                throw new System.InvalidOperationException();

            if (index < 0 || index >= Length)
                throw new System.IndexOutOfRangeException();

            var entity = m_Elements.IndexToEntity(index);
            return entity;
        }

        public ref CollectionElementComponent<TElement> GetContainerByRef(in int index)
        {
            var targetIndex = ValidateOperation(index);
            ref var component = ref ComponentMap<CollectionElementComponent<TElement>>.GetByRef(m_Elements.World, targetIndex);
            if (!component.HasValue)
                throw new System.InvalidOperationException();

            return ref component;
        }

        public CollectionPtrSlice<TElement> GetSlice(in int position, in int length) =>
            New(m_Elements.GetSlice(position, length));

        public CollectionPtr<TElement> CloneWithSize(in int length)
        {
            var newCollection = CollectionPtr<TElement>.Allocate(m_Elements.World, length);
            CopyTo(ref newCollection);
            return newCollection;
        }

        public void CopyTo(ref CollectionPtr<TElement> other, in int offset = 0)
        {
            for (var i = System.Math.Max(0, offset); i < System.Math.Min(Length, other.Length); i++)
                other[i] = Get(i);
        }

        public void CopyFrom(in CollectionPtrSlice<TElement> other, in int offset = 0)
        {
            for (var i = System.Math.Max(0, offset); i < System.Math.Min(Length, other.Length); i++)
                Set(i, other[i]);
        }

        public void Clear()
        {
            for (var i = 0; i < Length; i++)
                Set(i, default);
        }

        public TElement[] ToArray()
        {
            var result = new TElement[Length];
            for (var i = 0; i < result.Length; i++)
                result[i] = Get(i);

            return result;
        }

        public override string ToString() =>
            m_Elements.ToString();

        public static CollectionPtrSlice<TElement> New(in EntityRangePtr entityRange) =>
           new(entityRange);

        public static implicit operator EntityRangePtr(CollectionPtrSlice<TElement> slice) =>
            slice.m_Elements;
    }
}
