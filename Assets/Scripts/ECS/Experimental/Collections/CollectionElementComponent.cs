﻿namespace ECS.Core.Experimental.Collections
{
    [System.Serializable]
    public struct CollectionElementTagComponent : IComponent
    {
        void IData.WriteToDataLayout(in int world, in int entity) =>
            ComponentMap<CollectionElementTagComponent>.AddOrSet(world, entity, this);
    }

    [System.Serializable]
    public struct CollectionElementComponent<TElement> : IComponent where TElement : struct
    {
        [UnityEngine.SerializeField]
        Ptr.EntityPtr m_CollectionValidator;
        public bool IsValid => m_CollectionValidator.IsValid;

        public bool HasValue => m_HasValue;
        [UnityEngine.SerializeField]
        bool m_HasValue;

        public TElement Value => m_Value;
        [UnityEngine.SerializeField]
        TElement m_Value;

        public CollectionElementComponent(in Ptr.EntityPtr collectionValidator)
        {
            m_CollectionValidator = collectionValidator;
            m_HasValue = false;
            m_Value = default;
        }

        public CollectionElementComponent(in Ptr.EntityPtr collectionValidator, in TElement value)
        {
            m_CollectionValidator = collectionValidator;
            m_HasValue = true;
            m_Value = value;
        }

        public void Set(TElement element) =>
            SetInternal(element, true);

        public void Reset() =>
            SetInternal(default, false);

        void SetInternal(TElement value, bool isValid)
        {
            m_Value = value;
            m_HasValue = isValid;
        }

        void IData.WriteToDataLayout(in int world, in int entity) =>
            ComponentMap<CollectionElementComponent<TElement>>.AddOrSet(world, entity, this);
    }

    [System.AttributeUsage(System.AttributeTargets.Struct, AllowMultiple = false)]
    public class RequireCollectionOfTypeAttribute : RequireGenericComponenentTypeAttribute
    {
        public RequireCollectionOfTypeAttribute(params System.Type[] collectionTypes) : base(ToCollectionComponentTypes(collectionTypes)) { }

        static System.Type[] ToCollectionComponentTypes(System.Type[] collectionTypes)
        {
            var result = new System.Type[collectionTypes.Length];
            for(var i = 0; i < result.Length; i++)
                result[i] = typeof(CollectionElementComponent<>).MakeGenericType(collectionTypes[i]);

            return result;
        }
    }
}
