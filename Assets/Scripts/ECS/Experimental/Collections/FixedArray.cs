using UnityEngine;

namespace ECS.Core.Experimental.Collections
{
    public interface IFixedArray<TElement> where TElement : struct
    {
        int Capacity { get; }
        TElement this[in int index] { get; set; }
        TElement[] ToArray();
    }

    [System.Serializable]
    public struct FixedArray4<TElement> : IFixedArray<TElement> where TElement : struct
    {
        public const int CAPACITY = 4;

        int IFixedArray<TElement>.Capacity => CAPACITY;

        [SerializeField] TElement m_E0;
        [SerializeField] TElement m_E1;
        [SerializeField] TElement m_E2;
        [SerializeField] TElement m_E3;

        public TElement this[in int index]
        {
            get => Get(index);
            set => Set(index, value);
        }

        TElement Get(in int index) =>
            index switch
            {
                0 => m_E0,
                1 => m_E1,
                2 => m_E2,
                3 => m_E3,
                _ => throw new System.IndexOutOfRangeException(),
            };

        void Set(in int index, in TElement value)
        {
            switch (index)
            {
                default:
                    throw new System.IndexOutOfRangeException();
                case 0:
                    m_E0 = value;
                    break;
                case 1:
                    m_E1 = value;
                    break;
                case 2:
                    m_E2 = value;
                    break;
                case 3:
                    m_E3 = value;
                    break;
            }
        }

        public TElement[] ToArray() =>
            new TElement[] { m_E0, m_E1, m_E2, m_E3 };
    }

    [System.Serializable]
    public struct FixedArray8<TElement> : IFixedArray<TElement> where TElement : struct
    {
        public const int CAPACITY = 8;

        int IFixedArray<TElement>.Capacity => CAPACITY;

        [SerializeField] TElement m_E0;
        [SerializeField] TElement m_E1;
        [SerializeField] TElement m_E2;
        [SerializeField] TElement m_E3;
        [SerializeField] TElement m_E4;
        [SerializeField] TElement m_E5;
        [SerializeField] TElement m_E6;
        [SerializeField] TElement m_E7;

        public TElement this[in int index]
        {
            get => Get(index);
            set => Set(index, value);
        }

        TElement Get(in int index) =>
            index switch
            {
                0 => m_E0,
                1 => m_E1,
                2 => m_E2,
                3 => m_E3,
                4 => m_E4,
                5 => m_E5,
                6 => m_E6,
                7 => m_E7,
                _ => throw new System.IndexOutOfRangeException(),
            };

        void Set(in int index, in TElement value)
        {
            switch (index)
            {
                default:
                    throw new System.IndexOutOfRangeException();
                case 0:
                    m_E0 = value;
                    break;
                case 1:
                    m_E1 = value;
                    break;
                case 2:
                    m_E2 = value;
                    break;
                case 3:
                    m_E3 = value;
                    break;
                case 4:
                    m_E4 = value;
                    break;
                case 5:
                    m_E5 = value;
                    break;
                case 6:
                    m_E6 = value;
                    break;
                case 7:
                    m_E7 = value;
                    break;
            }
        }

        public TElement[] ToArray() =>
            new TElement[] { m_E0, m_E1, m_E2, m_E3, m_E4, m_E5, m_E6, m_E7 };
    }

    [System.Serializable]
    public struct FixedArrayBucket4<TElement, TBucket> : IFixedArray<TElement> where TElement : struct where TBucket : struct, IFixedArray<TElement>
    {
        public const int BUCKET_COUNT = 4;

        public int Capacity => m_B0.Capacity + m_B1.Capacity + m_B2.Capacity + m_B3.Capacity;

        [SerializeField] TBucket m_B0;
        [SerializeField] TBucket m_B1;
        [SerializeField] TBucket m_B2;
        [SerializeField] TBucket m_B3;

        public TElement this[in int index]
        {
            get => Get(index);
            set => Set(index, value);
        }

        TElement Get(in int index)
        {
            var bucketSize = Capacity / BUCKET_COUNT;
            var bucket = index / bucketSize;
            var indexInBucket = index % bucketSize;
            return bucket switch
            {
                0 => m_B0[indexInBucket],
                1 => m_B1[indexInBucket],
                2 => m_B1[indexInBucket],
                3 => m_B1[indexInBucket],
                _ => throw new System.IndexOutOfRangeException(),
            };
        }

        void Set(in int index, in TElement value)
        {
            var bucketSize = Capacity / BUCKET_COUNT;
            var bucket = index / bucketSize;
            var indexInBucket = index % bucketSize;
            switch (bucket)
            {
                default:
                    throw new System.IndexOutOfRangeException();
                case 0:
                    m_B0[indexInBucket] = value;
                    break;
                case 1:
                    m_B1[indexInBucket] = value;
                    break;
                case 2:
                    m_B1[indexInBucket] = value;
                    break;
                case 3:
                    m_B1[indexInBucket] = value;
                    break;
            }
        }

        public TElement[] ToArray()
        {
            var result = new TElement[Capacity];
            for (var i = 0; i < Capacity; i++)
                result[i] = Get(i);

            return result;
        }
    }

    [SerializeField]
    public struct FixedArray16<TElement> : IFixedArray<TElement> where TElement : struct
    {
        int IFixedArray<TElement>.Capacity => m_InternalArray.Capacity;

        [SerializeField] FixedArrayBucket4<TElement, FixedArray4<TElement>> m_InternalArray;

        public TElement this[in int index]
        {
            get => m_InternalArray[index];
            set => m_InternalArray[index] = value;
        }

        public TElement[] ToArray() =>
            m_InternalArray.ToArray();
    }

    [SerializeField]
    public struct FixedArray32<TElement> : IFixedArray<TElement> where TElement : struct
    {
        int IFixedArray<TElement>.Capacity => m_InternalArray.Capacity;

        [SerializeField] FixedArrayBucket4<TElement, FixedArray8<TElement>> m_InternalArray;

        public TElement this[in int index]
        {
            get => m_InternalArray[index];
            set => m_InternalArray[index] = value;
        }

        public TElement[] ToArray() =>
            m_InternalArray.ToArray();
    }

    [SerializeField]
    public struct FixedArray64<TElement> : IFixedArray<TElement> where TElement : struct
    {
        int IFixedArray<TElement>.Capacity => m_InternalArray.Capacity;

        [SerializeField] FixedArrayBucket4<TElement, FixedArray16<TElement>> m_InternalArray;

        public TElement this[in int index]
        {
            get => m_InternalArray[index];
            set => m_InternalArray[index] = value;
        }

        public TElement[] ToArray() =>
            m_InternalArray.ToArray();
    }

    [SerializeField]
    public struct FixedArray128<TElement> : IFixedArray<TElement> where TElement : struct
    {
        int IFixedArray<TElement>.Capacity => m_InternalArray.Capacity;

        [SerializeField] FixedArrayBucket4<TElement, FixedArray32<TElement>> m_InternalArray;

        public TElement this[in int index]
        {
            get => m_InternalArray[index];
            set => m_InternalArray[index] = value;
        }

        public TElement[] ToArray() =>
            m_InternalArray.ToArray();
    }
}