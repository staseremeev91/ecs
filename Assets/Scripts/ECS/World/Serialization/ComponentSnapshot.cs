﻿#define TYPE_AS_HASH

using System;
using UnityEngine;

namespace ECS
{
    [Serializable]
    public struct ComponentSnapshot
    {
#if TYPE_AS_HASH
        [SerializeField]
        int t;
#else
        [SerializeField]
        string t;
#endif

        [SerializeField]
        string d;

        public ComponentSnapshot(IData targetComponenent)
        {
#if TYPE_AS_HASH
            t = Core.DataTypeUtility.HashCodeOf(targetComponenent.GetType());
#else
            t = targetComponenent.GetType().FullName;
#endif
            d = JsonUtility.ToJson(targetComponenent);
        }

        public IData Restore()
        {
#if TYPE_AS_HASH
            Core.DataTypeUtility.TryGetType(t, out var componenentType);
#else
            var componenentType = Type.GetType(t);
#endif
            var result = JsonUtility.FromJson(d, componenentType);
            return (IData)result;
        }
    }
}
