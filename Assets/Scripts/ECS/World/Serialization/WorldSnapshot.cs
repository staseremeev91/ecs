﻿using ECS.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ECS
{
    [Serializable]
    public class WorldSnapshot
    {
        [SerializeField]
        int uid;
        public int UID => uid;

        [SerializeField]
        List<ComponentCollection> cd;
        public List<ComponentCollection> ComponentsData => cd;

        public WorldSnapshot(ECSWorld world) : this(world.UID) { }

        public WorldSnapshot(int worldUID)
        {
            uid = worldUID;

            cd = new List<ComponentCollection>();

            var componentSerializableAttributeCheck = new Dictionary<Type, bool>();

            foreach (var kvp in SharedComponentMap.GetWorldEntitiesCollection(worldUID))
            {
                var componentsData = new ComponentCollection(kvp.Key);
                foreach (var typeHash in kvp.Value)
                {
                    if (!DataTypeUtility.TryGetType(typeHash, out var type))
                        continue;

                    if (!componentSerializableAttributeCheck.TryGetValue(type, out var canBeSerialized))
                    {
                        canBeSerialized = Attribute.IsDefined(type, typeof(SerializableAttribute));
                        componentSerializableAttributeCheck.Add(type, canBeSerialized);
                    }

                    if (!canBeSerialized)
                        continue;

                    if (SharedComponentMap.TryGetData(worldUID, kvp.Key, type, out var component))
                        componentsData.Add(new ComponentSnapshot(component));
                }

                cd.Add(componentsData);
            }
        }

        public string ToJSON() =>
            JsonUtility.ToJson(this);

        [Serializable]
        public class ComponentCollection : IReadOnlyList<ComponentSnapshot>
        {
            public ComponentCollection(int entityRef)
            {
                e = entityRef;
                d = new List<ComponentSnapshot>();
            }

            [SerializeField]
            int e;
            public int Entity => e;

            [SerializeField]
            List<ComponentSnapshot> d;
            public ComponentSnapshot this[int index] => d[index];
            public int Count => d.Count;

            public void Add(ComponentSnapshot component) => d.Add(component);

            public IEnumerator<ComponentSnapshot> GetEnumerator() => d.GetEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
