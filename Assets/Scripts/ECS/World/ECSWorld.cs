﻿using ECS.Core;
using System.Collections.Generic;

namespace ECS
{
    public sealed class ECSWorld
    {
        public static int NextWorldUID => LastWorldUID + 1;
        static int LastWorldUID = 0;

        public static IReadOnlyCollection<ECSWorld> All => s_All.Values;
        static readonly Dictionary<int, ECSWorld> s_All = new();
        public static void DestroyAll()
        {
            foreach (var world in new List<ECSWorld>(All))
                world.Destroy();
        }

        public static bool TryGetByUID(in int worldUID, out ECSWorld world) =>
            s_All.TryGetValue(worldUID, out world);

        public int UID { get; private set; }
        public bool IsValid => UID > 0;

        public IReadOnlyDictionary<int, HashSet<int>> EntityComponetPairs =>
            SharedComponentMap.GetWorldEntitiesCollection(UID);

        public IEnumerable<int> Entities =>
            EntityComponetPairs.Keys;

        public int EntitiesCount =>
            EntityComponetPairs.Count;

        public readonly IRootSystemGroup RootSystem;

        ECSWorld(in IRootSystemGroup rootSystem, in int uid)
        {
            RootSystem = rootSystem;
            UID = uid;
            s_All.Add(UID, this);
        }

        public ECSWorld(in IRootSystemGroup rootSystem) : this(rootSystem, ++LastWorldUID) { }
        public ECSWorld(in IRootSystemGroup rootSystem, in WorldSnapshot snapshot) : this(rootSystem, snapshot.UID)
        {
            foreach (var componentCollection in snapshot.ComponentsData)
                foreach (var serializedComponent in componentCollection)
                    serializedComponent
                        .Restore()
                        .WriteToDataLayout(snapshot.UID, componentCollection.Entity);
        }

        public void ExecuteUpdate(float deltaTime) =>
            RootSystem.ExecuteUpdate(deltaTime);

        public void ExecuteGizmoUpdate(float deltaTime) =>
            RootSystem.ExecuteGizmoUpdate(deltaTime);

        public void UpdateFrom(DataMapSnapshot snapshot) =>
            SharedComponentMap.RestoreFromSnapshot(snapshot);

        public void Destroy()
        {
            SharedComponentMap.RemoveDataForWorld(UID);

            // DestroyRoot;
            DestroyView();

            // Invalidate id and remove world;
            s_All.Remove(UID);
            UID = 0;

            // Dispose systems.
            RootSystem.Dispose();
        }

        public void DestroyView() =>
            GameObjectByRef.Dispose(UID);

        public ReadWriteComponentAccessor<TComponent> GetComponentsWithEntity<TComponent>() where TComponent : struct, IComponent =>
            ComponentMap<TComponent>.GetReadWriteAccessor(UID);

        public ReadOnlyComponentAccessor<TComponent> GetComponentsWithEntityReadOnly<TComponent>() where TComponent : struct, IComponent =>
            ComponentMap<TComponent>.GetReadWriteAccessor(UID).ReadOnly;

        public WorldSnapshot ToSnapshot() =>
            new(this);

        public DataMapSnapshot ToComponentMapSnapshot() =>
            SharedComponentMap.BuildSnapshotForWorld(UID);

        public Core.Experimental.Collections.CollectionPtr<TElement> AllocateCollection<TElement>(in int length) where TElement : unmanaged =>
            Core.Experimental.Collections.CollectionPtr<TElement>.AllocateViaCommandBuffer(UID, length, new DirectCommandBufferConcatenator());

        public Core.Experimental.Collections.CollectionPtr<TElement> AllocateCollectionViaCommandBuffer<TElement>(in int length, in ICommandBufferConcatenator commandBuffer) where TElement : unmanaged =>
            Core.Experimental.Collections.CollectionPtr<TElement>.AllocateViaCommandBuffer(UID, length, commandBuffer);

        public bool TryGetSystem<TSystem>(out TSystem system) where TSystem : class, ISystem
        {
            var systemExists = RootSystem.TryGetSystem(out system);
            if (!systemExists)
                UnityEngine.Debug.LogError($"System of type '{typeof(TSystem).FullName}' not registered in world with uid '{UID}'");

            return systemExists;
        }
    }
}