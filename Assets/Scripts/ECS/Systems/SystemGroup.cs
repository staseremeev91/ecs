﻿using System.Collections.Generic;

namespace ECS.Core
{
    public interface ISystemGroup : ISystemBase
    {
        IReadOnlyCollection<ISystem> Systems { get; }
        bool TryGetSystem<TSystem>(out TSystem system) where TSystem : class, ISystem;
    }
}

namespace ECS
{
    public class SystemGroup : Core.BaseSystem, Core.ISystemGroup
    {
        IReadOnlyCollection<Core.ISystem> Core.ISystemGroup.Systems => m_Systems.Values;

        readonly Dictionary<int, Core.ISystem> m_Systems;

        public SystemGroup(in int worldUID, params Core.ISystem[] systems) : base(worldUID)
        {
            m_Systems = new Dictionary<int, Core.ISystem>();
            foreach (var system in systems)
            {
                var systemHashCode = system.GetType().GetHashCode();
                if (m_Systems.ContainsKey(systemHashCode))
                    throw new System.InvalidOperationException();

                m_Systems.Add(systemHashCode, system);
            }
        }

        bool Core.ISystemGroup.TryGetSystem<TSystem>(out TSystem system)
        {
            system = null;

            if (m_Systems.TryGetValue(typeof(TSystem).GetHashCode(), out var boxedSystem))
                system = boxedSystem as TSystem;

            if(system == null)
            {
                foreach(var childSystem in m_Systems.Values)
                    if (childSystem is Core.ISystemGroup groupSystem && groupSystem.TryGetSystem(out system))
                        break;
            }

            return system != null;
        }

        protected sealed override void OnExecuteUpdate(in float deltaTime)
        {
            base.OnExecuteUpdate(deltaTime);
            foreach (var system in m_Systems.Values)
                system.ExecuteUpdate(deltaTime);
        }

        void Core.ISystemBase.ExecuteGizmoUpdate(in float deltaTime)
        {
            foreach (var system in m_Systems.Values)
                system.ExecuteGizmoUpdate(deltaTime);
        }

        public override void Dispose()
        {
            base.Dispose();

            foreach (var system in m_Systems.Values)
                system.Dispose();

            m_Systems.Clear();
        }
    }
}
