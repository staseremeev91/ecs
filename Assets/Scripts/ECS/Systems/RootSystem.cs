using System;
using ECS.Core;

namespace ECS.Core
{
    public interface IRootSystemGroup : ISystemGroup
    {
    }
}

namespace ECS
{
    public sealed class RootSystem : SystemGroup, IRootSystemGroup
    {
        private int m_LastEntityUID;

        public RootSystem(in int worldUID, params ISystem[] systems) : base(worldUID, systems)
        {
            SharedComponentMap.ComponentAdded += OnComponentAdded;
            SharedComponentMap.WorldRestoredFromSnapshot += OnWorldRestoredFromSnapshot;

            Reset();
        }

        private void OnWorldRestoredFromSnapshot(in int world)
        {
            if (world == WorldUID)
                Reset();
        }

        private void OnComponentAdded(in int world, in int entity, in int componentTypeHash, in bool newEntityAllocated)
        {
            if (!InExecutionContext || !newEntityAllocated || world != WorldUID)
                return;

            if (entity <= m_LastEntityUID)
            {
                var componentDescription = TypeLookup.TryGetType(componentTypeHash, out var componentType) ? componentType.FullName : componentTypeHash.ToString();
                var message = $"Trying to instantiate entity with id '{entity}' while executing '{CurrentSystemContext.FullName}' but it was already destroyed. Creation reason: '{componentDescription}' component added.";
                // TODO: Implement journaling and get entity destroy information: system, ets.
                throw new InvalidOperationException(message);
            }

            m_LastEntityUID = entity;
        }

        public void Reset() =>
            m_LastEntityUID = 0;

        public override void Dispose()
        {
            base.Dispose();
            SharedComponentMap.ComponentAdded -= OnComponentAdded;
            SharedComponentMap.WorldRestoredFromSnapshot -= OnWorldRestoredFromSnapshot;
        }
    }
}
