using System;

namespace ECS.Core
{
    public interface ISystemBase : IDisposable
    {
        bool Enabled { get; set; }

        void ExecuteUpdate(in float deltaTime);
        void ExecuteGizmoUpdate(in float deltaTime);

#if UNITY_EDITOR
        double LastUpdateTimeMS_Editor { get; }
#endif
    }
}

