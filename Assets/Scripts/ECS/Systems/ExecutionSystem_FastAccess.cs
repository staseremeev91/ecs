namespace ECS
{
    public abstract class ExecutionSystem<T0> : ExecutionSystem
        where T0 : struct, IComponent
    {
        readonly ISystemEntityQuery m_Query;
        readonly ReadWriteComponentAccessor<T0> m_T0Provider;

        protected ExecutionSystem(in int worldUID) : base(worldUID)
        {
            m_Query =
                GetQueryBuilder()
                    .All()
                    .OfWritableComponent(out m_T0Provider)
                    .ToQuery();
        }

        protected sealed override void OnExecuteUpdate(in float deltaTime)
        {
            base.OnExecuteUpdate(deltaTime);
            foreach (var targetEntity in m_Query.GetEntities())
                OnUpdate(targetEntity, ref m_T0Provider.GetByRef(targetEntity));
        }

        protected abstract void OnUpdate(in int entity, ref T0 t0Component);
    }

    public abstract class ExecutionSystem<T0, T1> : ExecutionSystem
        where T0 : struct, IComponent
        where T1 : struct, IComponent
    {
        readonly ISystemEntityQuery m_Query;
        readonly ReadWriteComponentAccessor<T0> m_T0Provider;
        readonly ReadWriteComponentAccessor<T1> m_T1Provider;

        protected ExecutionSystem(in int worldUID) : base(worldUID)
        {
            m_Query =
                GetQueryBuilder()
                    .All()
                    .OfWritableComponent(out m_T0Provider)
                    .OfWritableComponent(out m_T1Provider)
                    .ToQuery();
        }

        protected sealed override void OnExecuteUpdate(in float deltaTime)
        {
            base.OnExecuteUpdate(deltaTime);
            foreach (var targetEntity in m_Query.GetEntities())
                OnUpdate(targetEntity,
                    ref m_T0Provider.GetByRef(targetEntity),
                    ref m_T1Provider.GetByRef(targetEntity));
        }

        protected abstract void OnUpdate(in int entity, ref T0 t0Component, ref T1 t1Component);
    }

    public abstract class ExecutionSystem<T0, T1, T2> : ExecutionSystem
        where T0 : struct, IComponent
        where T1 : struct, IComponent
        where T2 : struct, IComponent

    {
        readonly ISystemEntityQuery m_Query;
        readonly ReadWriteComponentAccessor<T0> m_T0Provider;
        readonly ReadWriteComponentAccessor<T1> m_T1Provider;
        readonly ReadWriteComponentAccessor<T2> m_T2Provider;

        protected ExecutionSystem(in int worldUID) : base(worldUID)
        {
            m_Query =
                GetQueryBuilder()
                    .All()
                    .OfWritableComponent(out m_T0Provider)
                    .OfWritableComponent(out m_T1Provider)
                    .OfWritableComponent(out m_T2Provider)
                    .ToQuery();
        }

        protected sealed override void OnExecuteUpdate(in float deltaTime)
        {
            base.OnExecuteUpdate(deltaTime);
            foreach (var targetEntity in m_Query.GetEntities())
                OnUpdate(targetEntity,
                    ref m_T0Provider.GetByRef(targetEntity),
                    ref m_T1Provider.GetByRef(targetEntity),
                    ref m_T2Provider.GetByRef(targetEntity));
        }

        protected abstract void OnUpdate(in int entity, ref T0 t0Component, ref T1 t1Component, ref T2 t2Component);
    }
}
