﻿using System;
using System.Collections.Generic;

namespace ECS
{
    public abstract class ExecutionSystem : Core.BaseSystem, Core.ISystem
    {
        public IReadOnlyList<ISystemEntityQuery> TrackedQueries => m_TrackedQueries;
        readonly List<IDisposableSystemEntityQuery> m_TrackedQueries;

        public IReadOnlyList<ISystemEntityQuery> RequiredForUpdateQueries => m_RequiredForUpdateQueries;
        readonly List<ISystemEntityQuery> m_RequiredForUpdateQueries;

        protected ExecutionSystem(in int worldUID) : base(worldUID)
        {
            m_TrackedQueries = new List<IDisposableSystemEntityQuery>();
            m_RequiredForUpdateQueries = new List<ISystemEntityQuery>();

            Core.SharedComponentMap.ComponentAdded += OnComponenentAdded;
            Core.SharedComponentMap.ComponentRemoved += OnComponenentRemoved;

            Core.SharedComponentMap.ComponentsRemovedForWorld += OnComponentsRemovedForWorld;
            Core.SharedComponentMap.ComponentsRemovedForEntity += OnComponentsRemovedForEntity;
        }


        protected IEntityQueryBuilder GetQueryBuilder(in bool includeeDisabled = false)
        {
            if (InExecutionContext)
                throw new InvalidOperationException("Unable to build the query while executing system.");

            return SystemEntityQueryBuilder.New(WorldUID, queryHandler: this, includeeDisabled);
        }


        protected ReadWriteSingletonComponentAccessor<TComponent> GetSingletonAccessor<TComponent>(in bool requiredForUpdate = false) where TComponent : struct, IComponent
        {
            GetQueryBuilder()
                .All()
                .OfWritableSingltonComponent<TComponent>(out var accessor)
                .ToQuery(requiredForUpdate);
          
            return accessor;
        }

        protected ReadOnlySingletonComponentAccessor<TComponent> GetReadOnlySingletonAccessor<TComponent>(in bool requiredForUpdate = false) where TComponent : struct, IComponent
        {
            GetQueryBuilder()
                .All()
                .OfReadableOnlySingletonComponent<TComponent>(out var accessor)
                .ToQuery(requiredForUpdate);

            return accessor;
        }
            

        protected ISystemEntityQuery GetEventEntityQuery<TEvent>(out ReadOnlyComponentAccessor<TEvent> collection, in bool requiredForUpdate = false) where TEvent : struct, Core.IEventComponent =>
            GetQueryBuilder()
                .All()
                .OfReadableOnlyComponent(out collection)
                .ToQuery(requiredForUpdate);

        void ISystemQueryHandler.RegisterQuery(in IDisposableSystemEntityQuery query, in bool requireForUpdate)
        {
            m_TrackedQueries.Add(query);
            if (requireForUpdate)
                m_RequiredForUpdateQueries.Add(query);
        }

        protected override bool CanBeUpdated()
        {
            if (!base.CanBeUpdated())
                return false;

            foreach (var query in m_RequiredForUpdateQueries)
                if (query.IsEmpty())
                    return false;

            return true;
        }

        #region STRUCTURAL_VALIDATION
        void OnComponenentAdded(in int world, in int entity, in int componentTypeHash, in bool newEntityAdded) =>
            ValidateStructuralChanges(world);

        void OnComponenentRemoved(in int world, in int entity, in int componentTypeHash) =>
            ValidateStructuralChanges(world);

        void OnComponentsRemovedForEntity(in int world, in int entity) =>
            ValidateStructuralChanges(world);

        void OnComponentsRemovedForWorld(in int world) =>
            ValidateStructuralChanges(world);

        void ValidateStructuralChanges(in int world)
        {
            if (InExecutionContext && world == WorldUID)
                throw new InvalidOperationException($"[{GetType().FullName}] Structural changes are not possible to make during system update execution. Use '{nameof(PreUpdateCommandBuffer)}' or '{nameof(PostUpdateCommandBuffer)}' insteaad.");
        }
        #endregion

        public override void Dispose()
        {
            base.Dispose();

            Core.SharedComponentMap.ComponentAdded -= OnComponenentAdded;
            Core.SharedComponentMap.ComponentRemoved -= OnComponenentRemoved;

            Core.SharedComponentMap.ComponentsRemovedForWorld -= OnComponentsRemovedForWorld;
            Core.SharedComponentMap.ComponentsRemovedForEntity -= OnComponentsRemovedForEntity;

            foreach (var query in m_TrackedQueries)
                query.Dispose();

            m_TrackedQueries.Clear();
            m_RequiredForUpdateQueries.Clear();
        }
    }
}
