﻿using System.Collections.Generic;

namespace ECS.Core
{
    public interface ISystem : ISystemBase, ISystemQueryHandler
    {
        IReadOnlyList<ISystemEntityQuery> TrackedQueries { get; }
        IReadOnlyList<ISystemEntityQuery> RequiredForUpdateQueries { get; }
    }
}
