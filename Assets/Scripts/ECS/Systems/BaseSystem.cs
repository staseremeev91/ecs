namespace ECS.Core
{
    public abstract class BaseSystem : ISystemBase
    {
#if UNITY_EDITOR
        public static System.Type CurrentSystemContext { get; private set; }
#endif

        private bool m_Enabled;
        public bool Enabled
        {
            get => m_Enabled;
            set
            {
                if (InExecutionContext)
                    throw new System.InvalidOperationException("Unable to changed systems state from it's execution context.");

                m_Enabled = value;
            }
        }

        public readonly int WorldUID;

        public ICommandBufferOperator PreUpdateCommandBuffer => m_PreUpdateCommandBuffer;
        readonly CommandBuffer m_PreUpdateCommandBuffer;

        public ICommandBufferOperator PostUpdateCommandBuffer => m_PostUpdateCommandBuffer;
        readonly CommandBuffer m_PostUpdateCommandBuffer;

        protected bool InExecutionContext { get; private set; }
        protected bool InUpdateExecutionContext { get; private set; }

        bool m_FirstUpdateCalled;

#if UNITY_EDITOR
        public double LastUpdateTimeMS_Editor { get; private set; }
        readonly System.Diagnostics.Stopwatch m_Watch;
#endif

        protected BaseSystem(in int worldUID)
        {
            Enabled = true;

            WorldUID = worldUID;

            m_PreUpdateCommandBuffer = new CommandBuffer();
            m_PostUpdateCommandBuffer = new CommandBuffer();

#if UNITY_EDITOR
            m_Watch = new System.Diagnostics.Stopwatch();
#endif
        }

        protected void PostEvent<TEvent>(in TEvent eventComponentData) where TEvent : struct, IEventComponent
        {
            if (!InUpdateExecutionContext)
                throw new System.InvalidOperationException($"Unable to post event out of the systems update execution context. Use 'OnExecuteUpdate' function to post events.");

            var newEntity = EntityAllocatorComponenent.Allocate(WorldUID);
            // Create event entity after systems gets updated.
            PostUpdateCommandBuffer.TryAddOrSetComponent(WorldUID, newEntity, eventComponentData);
            // Destroy event entity on next update iteration.
            PreUpdateCommandBuffer.TryRemoveEntity(WorldUID, newEntity);
        }

        void ISystemBase.ExecuteUpdate(in float deltaTime)
        {
#if UNITY_EDITOR
            var lastUpdateContext = CurrentSystemContext;
            CurrentSystemContext = GetType();

            LastUpdateTimeMS_Editor = 0;
#endif
            if (!Enabled)
            {
                if (m_PreUpdateCommandBuffer.CommandsCount > 0 || m_PostUpdateCommandBuffer.CommandsCount > 0)
                {
                    UnityEngine.Debug.LogError($"[{GetType().FullName}] Unable to execute scheduled commands on disabled system.");

                    m_PreUpdateCommandBuffer.Clear();
                    m_PostUpdateCommandBuffer.Clear();
                }
                return;
            }

            var canBeUpdated = CanBeUpdated();

#if UNITY_EDITOR
            m_Watch.Restart();
#endif
            // Start execution context.
            if (canBeUpdated)
            {
                InExecutionContext = true;
                {
                    if (!m_FirstUpdateCalled)
                        OnExecuteStart();
                }
                InExecutionContext = false;
            }

            // Execute pre update command buffer.
            m_PreUpdateCommandBuffer.Execute();

            if (canBeUpdated)
            {
                // Update execution context.
                InExecutionContext = true;
                {
                    InUpdateExecutionContext = true;
                    OnExecuteUpdate(deltaTime);
                    InUpdateExecutionContext = false;

                    m_FirstUpdateCalled = true;
                }
                InExecutionContext = false;
            }

            // Execute post update command buffer.
            m_PostUpdateCommandBuffer.Execute();

#if UNITY_EDITOR
            LastUpdateTimeMS_Editor = m_Watch.Elapsed.TotalMilliseconds;
            CurrentSystemContext = lastUpdateContext;
#endif
        }

        void ISystemBase.ExecuteGizmoUpdate(in float deltaTime) =>
            OnExecuteGizmoUpdate(deltaTime);

        protected virtual bool CanBeUpdated() =>
            Enabled;

        protected virtual void OnExecuteStart() { }
        protected virtual void OnExecuteUpdate(in float deltaTime) { }
        protected virtual void OnExecuteGizmoUpdate(in float deltaTime) { }

        public virtual void Dispose()
        {
            m_PreUpdateCommandBuffer.Dispose();
            m_PostUpdateCommandBuffer.Dispose();
        }
    }

    public interface IEventComponent : IComponent { }
}
