using System.Collections.Generic;

namespace ECS
{
    public class EntityQueryBuilder
    {
        public static IDisposableEntityQuery NewQueryForAllEntities(in bool includeDisabled = false)
        {
            var includedAnyComponents = new HashSet<int>(Core.DataTypeUtility.Types.Keys);
            var includedAllComponents = new HashSet<int>();
            var excludedComponents = new HashSet<int>();

            if (!includeDisabled)
                excludedComponents.Add(Core.DataTypeUtility.HashCodeOf<DisabledComponent>());

            return EntityQuery.New(includedAnyComponents, includedAllComponents, excludedComponents);
        }
    }
}
