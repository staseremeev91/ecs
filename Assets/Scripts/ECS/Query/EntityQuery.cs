﻿using System;
using System.Collections.Generic;

namespace ECS
{
    public interface IEntityQueryDefinition
    {
        IReadOnlyCollection<int> Any { get; }
        IReadOnlyCollection<int> All { get; }
        IReadOnlyCollection<int> Exclude { get; }
    }

    public interface IRebuildableEntityQuery
    {
        void ForceRebuild();
    }

    public interface IEntityQuery : IEntityQueryDefinition, IRebuildableEntityQuery
    {
        IReadOnlyCollection<int> GetEntities(in int world);
        int Count(in int world);
    }

    public interface IDisposableEntityQuery : IEntityQuery, IDisposable
    {
    }    

    public sealed class EntityQuery : IDisposableEntityQuery
    {
        class WorldEntityCollection
        {
            readonly Dictionary<int, HashSet<int>> m_EntitiesByWorld;

            public WorldEntityCollection() =>
                m_EntitiesByWorld = new Dictionary<int, HashSet<int>>();

            public bool Contains(in int world, in int entity) =>
                ForWorld(world).Contains(entity);

            public void Add(in int world, in int entity) =>
                ForWorld(world).Add(entity);

            public void Remove(in int world, in int entity)
            {
                if (m_EntitiesByWorld.TryGetValue(world, out var entities))
                    entities.Remove(entity);
            }

            public void RemoveForWorld(in int world)
            {
                if (m_EntitiesByWorld.TryGetValue(world, out var entities))
                    entities.Clear();
            }

            public HashSet<int> ForWorld(in int world)
            {
                if (!m_EntitiesByWorld.TryGetValue(world, out var entities))
                    m_EntitiesByWorld.Add(world, entities = new HashSet<int>());

                return entities;
            }

            public void Clear()
            {
                foreach (var entities in m_EntitiesByWorld.Values)
                    entities.Clear();

                m_EntitiesByWorld.Clear();
            }
        }

        readonly HashSet<int> m_Any;
        readonly HashSet<int> m_All;
        readonly HashSet<int> m_Exclude;

        readonly WorldEntityCollection m_EntitiesByWorld;
        readonly WorldEntityCollection m_IncludedEntities;
        readonly WorldEntityCollection m_ExcludedEntities;

        public IReadOnlyCollection<int> Any =>
            m_Any;

        public IReadOnlyCollection<int> All =>
            m_All;

        public IReadOnlyCollection<int> Exclude =>
            m_Exclude;

        public static EntityQuery New(in HashSet<int> any, in HashSet<int> all, in HashSet<int> exclude) =>
            new(any, all, exclude);

        EntityQuery(in HashSet<int> any, in HashSet<int> all, in HashSet<int> exclude)
        {
            if (all.Overlaps(any) || all.Overlaps(exclude) || any.Overlaps(exclude))
                throw new InvalidOperationException("Unable to construct query with overlaped input component sets");

            m_Any = any;
            m_All = all;
            m_Exclude = exclude;

            foreach (var component in m_All)
                m_Any.Remove(component);

            m_EntitiesByWorld = new WorldEntityCollection();
            m_IncludedEntities = new WorldEntityCollection();
            m_ExcludedEntities = new WorldEntityCollection();

            Core.SharedComponentMap.ComponentAdded += OnComponenentAdded;
            Core.SharedComponentMap.ComponentRemoved += OnComponenentRemoved;

            Core.SharedComponentMap.ComponentsRemovedForWorld += OnComponentsRemovedForWorld;
            Core.SharedComponentMap.ComponentsRemovedForEntity += OnComponentsRemovedForEntity;
        }

        public void Dispose()
        {
            m_EntitiesByWorld.Clear();

            m_Any.Clear();
            m_All.Clear();
            m_Exclude.Clear();

            Core.SharedComponentMap.ComponentAdded -= OnComponenentAdded;
            Core.SharedComponentMap.ComponentRemoved -= OnComponenentRemoved;

            Core.SharedComponentMap.ComponentsRemovedForWorld -= OnComponentsRemovedForWorld;
            Core.SharedComponentMap.ComponentsRemovedForEntity -= OnComponentsRemovedForEntity;
        }

        public int Count(in int world) =>
            GetEntities(world).Count;

        public IReadOnlyCollection<int> GetEntities(in int world) =>
            m_EntitiesByWorld.ForWorld(world);

        /// <summary>
        /// Rebuild the whole query, could be too slow.
        /// TODO: Make it faster somehow, use archetypes to compare with.
        /// </summary>
        public void ForceRebuild()
        {
            m_EntitiesByWorld.Clear();
            m_IncludedEntities.Clear();
            m_ExcludedEntities.Clear();

            foreach (var world in Core.SharedComponentMap.Worlds)
            {
                var entities = Core.SharedComponentMap.GetWorldEntitiesCollection(world);
                foreach (var entity in entities)
                    foreach (var component in entity.Value)
                        OnComponenentAdded(world, entity.Key, component, newEntityAdded: false);
            }
        }

        void OnComponenentAdded(in int world, in int entity, in int componentTypeHash, in bool newEntityAdded)
        {
            if (m_All.Contains(componentTypeHash) || m_Any.Contains(componentTypeHash))
            {
                // Check 'ALL' group subset.
                if (Core.SharedComponentMap.CheckSubsetCollision(world, entity, m_All))
                {
                    // Check 'ANY' group subset, if > 0 components defined.
                    if (m_Any.Count == 0 || Core.SharedComponentMap.CheckCollision(world, entity, m_Any))
                        m_IncludedEntities.Add(world, entity);
                }
            }

            if (m_Exclude.Contains(componentTypeHash))
                m_ExcludedEntities.Add(world, entity);

            UpdateQuery(world, entity);
        }

        void OnComponenentRemoved(in int world, in int entity, in int componentTypeHash)
        {
            if (m_All.Contains(componentTypeHash))
                m_IncludedEntities.Remove(world, entity);

            if (m_Any.Contains(componentTypeHash))
            {
                if (!Core.SharedComponentMap.CheckCollision(world, entity, m_Any))
                    m_IncludedEntities.Remove(world, entity);
            }

            if (m_Exclude.Contains(componentTypeHash))
            {
                if (!Core.SharedComponentMap.CheckCollision(world, entity, m_Exclude))
                    m_ExcludedEntities.Remove(world, entity);
            }

            UpdateQuery(world, entity);
        }

        void UpdateQuery(in int world, in int entity)
        {
            if (m_IncludedEntities.Contains(world, entity) && !m_ExcludedEntities.Contains(world, entity))
                m_EntitiesByWorld.Add(world, entity);
            else
                m_EntitiesByWorld.Remove(world, entity);
        }

        void OnComponentsRemovedForWorld(in int world)
        {
            m_IncludedEntities.RemoveForWorld(world);
            m_ExcludedEntities.RemoveForWorld(world);
            m_EntitiesByWorld.RemoveForWorld(world);
        }

        void OnComponentsRemovedForEntity(in int world, in int entity)
        {
            m_IncludedEntities.Remove(world, entity);
            m_ExcludedEntities.Remove(world, entity);
            m_EntitiesByWorld.Remove(world, entity);
        }
    }
}
