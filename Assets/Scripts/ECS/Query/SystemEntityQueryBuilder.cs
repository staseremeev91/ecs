﻿using System;
using System.Collections.Generic;
using static ECS.SystemEntityQueryBuilderDescriptors;

namespace ECS
{
    public class SystemEntityQueryBuilder : IEntityQueryBuilder
    {
        readonly Dictionary<int, QueryComponentOptions> m_ComponentOptions;

        readonly EntityQueryBuilderGroup m_IncludedAny;
        readonly EntityQueryBuilderGroup m_IncludedAll;
        readonly EntityQueryBuilderGroup m_Excluded;
        readonly EntityQueryBuilderGroup m_Optional;

        readonly int m_WorldUID;
        ISystemQueryHandler m_QueryHandler;

        bool m_Builded;

        public static IEntityQueryBuilder New(in int worldUID, in ISystemQueryHandler queryHandler, in bool includeDisabled) =>
            new SystemEntityQueryBuilder(worldUID, queryHandler, includeDisabled);

        SystemEntityQueryBuilder(in int worldUID, in ISystemQueryHandler queryHandler, in bool includeDisabled)
        {
            m_WorldUID = worldUID;
            m_ComponentOptions = new Dictionary<int, QueryComponentOptions>();

            m_IncludedAny = new EntityQueryBuilderGroup(m_ComponentOptions, builder: this, baseOptions: QueryComponentOptions.IncludeAny);
            m_IncludedAll = new EntityQueryBuilderGroup(m_ComponentOptions, builder: this, baseOptions: QueryComponentOptions.IncludeAll);
            m_Excluded = new EntityQueryBuilderGroup(m_ComponentOptions, builder: this, baseOptions: QueryComponentOptions.Exclude);
            m_Optional = new EntityQueryBuilderGroup(m_ComponentOptions, builder: this, baseOptions: QueryComponentOptions.Optional);

            if (!includeDisabled)
                m_Excluded.Components.Add(Core.DataTypeUtility.HashCodeOf<DisabledComponent>());

            m_QueryHandler = queryHandler;
            m_Builded = false;
        }

        int IEntityQueryBuilder.WorldUID =>
            m_WorldUID;

        IGroup_Default IEntityQueryBuilder.Any() =>
            m_IncludedAny;

        IGroup_Default IEntityQueryBuilder.All() =>
            m_IncludedAll;

        IGroup_None IEntityQueryBuilder.None() =>
            m_Excluded;

        IGroup_Optional IEntityQueryBuilder.Optional() =>
           m_Optional;

        IFinalizer IEntityQueryBuilder.IncludeAllEntities()
        {
            m_IncludedAll.Components.Clear();
            m_Excluded.Components.Clear();
            m_IncludedAny.Components.UnionWith(Core.DataTypeUtility.Types.Keys);
            return this;
        }

        SystemEntityQuery ToQueryInternal(in bool requireForUpdate)
        {
            if (m_Builded)
                throw new InvalidOperationException($"{GetType().Name} is not valid anymore");

            m_Builded = true;

            var result = new SystemEntityQuery(m_WorldUID, m_IncludedAny.Components, m_IncludedAll.Components, m_Excluded.Components, m_Optional.Components, m_ComponentOptions);
            m_QueryHandler.RegisterQuery(result, requireForUpdate);
            m_QueryHandler = null;

            return result;
        }

        IDisposableSystemEntityQuery IFinalizer.ToQuery(in bool requireForUpdate) =>
            ToQueryInternal(requireForUpdate);

        IDisposableSystemEntityQuery IFinalizer.ToQueryAndRebuild(in bool requireForUpdate)
        {
            var query = ToQueryInternal(requireForUpdate);
            query.ForceRebuild();
            return query;
        }

        class EntityQueryBuilderGroup : IGroup_Default, IGroup_Optional, IGroup_None
        {
            readonly IEntityQueryBuilder m_Builder;

            public readonly HashSet<int> Components;

            readonly IDictionary<int, QueryComponentOptions> m_ComponentOptions;
            readonly QueryComponentOptions m_GroupOptions;

            public EntityQueryBuilderGroup(in IDictionary<int, QueryComponentOptions> componentOptions, in IEntityQueryBuilder builder, in QueryComponentOptions baseOptions)
            {
                Components = new HashSet<int>();

                m_Builder = builder;
                m_ComponentOptions = componentOptions;
                m_GroupOptions = baseOptions;
            }

            IGroup_Default IDFilter<IGroup_Default>.OfComponent<TComponent>()
            {
                var componentTypeHash = Core.DataTypeUtility.HashCodeOf<TComponent>();
                Components.Add(componentTypeHash);

                RegisterComponentOption(componentTypeHash, m_GroupOptions);
                return this;
            }

            IGroup_Default IDWritable<IGroup_Default>.OfWritableComponent<TComponent>(out ReadWriteComponentAccessor<TComponent> componentAccessor)
            {
                componentAccessor = ProvideReadWriteComponentAccessor<TComponent>(m_GroupOptions | QueryComponentOptions.Read | QueryComponentOptions.Write);
                return this;
            }

            IGroup_Default IDReadableOnly<IGroup_Default>.OfReadableOnlyComponent<TComponent>(out ReadOnlyComponentAccessor<TComponent> componentAccessor)
            {
                componentAccessor = ProvideReadWriteComponentAccessor<TComponent>(m_GroupOptions | QueryComponentOptions.Read).ReadOnly;
                return this;
            }

            IGroup_Optional IDReadableOnly<IGroup_Optional>.OfReadableOnlyComponent<TComponent>(out ReadOnlyComponentAccessor<TComponent> componentAccessor)
            {
                componentAccessor = ProvideReadWriteComponentAccessor<TComponent>(m_GroupOptions | QueryComponentOptions.Read).ReadOnly;
                return this;
            }

            IGroup_None IDFilter<IGroup_None>.OfComponent<TComponent>()
            {
                var componentTypeHash = Core.DataTypeUtility.HashCodeOf<TComponent>();
                Components.Add(componentTypeHash);

                RegisterComponentOption(componentTypeHash, m_GroupOptions);
                return this;
            }

            IEntityQueryBuilder IBackToBulder.BackToBuilder() =>
                m_Builder;

            IDisposableSystemEntityQuery IFinalizer.ToQuery(in bool requireForUpdate) =>
                m_Builder.ToQuery(requireForUpdate);

            IDisposableSystemEntityQuery IFinalizer.ToQueryAndRebuild(in bool requireForUpdate) =>
                m_Builder.ToQueryAndRebuild(requireForUpdate);

            void RegisterComponentOption(in int componentTypeHash, in QueryComponentOptions option)
            {
                if (m_ComponentOptions.ContainsKey(componentTypeHash))
                    m_ComponentOptions[componentTypeHash] |= option;
                else
                    m_ComponentOptions.Add(componentTypeHash, option);
            }

            IGroup_Default IDWritable<IGroup_Default>.OfWritableSingltonComponent<TComponent>(out ReadWriteSingletonComponentAccessor<TComponent> componentAccessor)
            {
                componentAccessor = ProvideReadWriteSingletonComponentAccessor<TComponent>(m_GroupOptions | QueryComponentOptions.Read | QueryComponentOptions.Write);
                return this;
            }

            IGroup_Default IDReadableOnly<IGroup_Default>.OfReadableOnlySingletonComponent<TComponent>(out ReadOnlySingletonComponentAccessor<TComponent> componentAccessor)
            {
                componentAccessor = ProvideReadWriteSingletonComponentAccessor<TComponent>(m_GroupOptions | QueryComponentOptions.Read).ReadOnly;
                return this;
            }

            IGroup_Optional IDReadableOnly<IGroup_Optional>.OfReadableOnlySingletonComponent<TComponent>(out ReadOnlySingletonComponentAccessor<TComponent> componentAccessor)
            {
                componentAccessor = ProvideReadWriteSingletonComponentAccessor<TComponent>(m_GroupOptions | QueryComponentOptions.Read).ReadOnly;
                return this;
            }

            ReadWriteComponentAccessor<TComponent> ProvideReadWriteComponentAccessor<TComponent>(QueryComponentOptions options) where TComponent : struct, IComponent
            {
                var componentAccessor = Core.ComponentMap<TComponent>.GetReadWriteAccessor(m_Builder.WorldUID);
                var componentTypeHash = Core.DataTypeUtility.HashCodeOf<TComponent>();
                Components.Add(componentTypeHash);

                RegisterComponentOption(componentTypeHash, options);
                return componentAccessor;
            }

            ReadWriteSingletonComponentAccessor<TComponent> ProvideReadWriteSingletonComponentAccessor<TComponent>(QueryComponentOptions options) where TComponent : struct, IComponent
            {
                var componentAccessor = Core.ComponentMap<TComponent>.GetReadWriteSingltonAccessor(m_Builder.WorldUID);
                var componentTypeHash = Core.DataTypeUtility.HashCodeOf<TComponent>();
                Components.Add(componentTypeHash);

                RegisterComponentOption(componentTypeHash, options);
                return componentAccessor;
            }
        }
    }
   
    public interface IEntityQueryBuilder : IFinalizer
    {
        int WorldUID { get; }

        IGroup_Default All();
        IGroup_Default Any();
        IGroup_None None();
        IGroup_Optional Optional();
        IFinalizer IncludeAllEntities();
    }

    [Flags]
    public enum QueryComponentOptions : int
    {
        None = 0,
        IncludeAll = 1 << 0,
        IncludeAny = 1 << 1,
        Exclude = 1 << 2,
        Optional = 1 << 3,
        Read = 1 << 4,
        Write = 1 << 5,
    }

    public static class SystemEntityQueryBuilderDescriptors
    {
        public interface IGroup_Default : IBase, IDFilter<IGroup_Default>, IDWritable<IGroup_Default>, IDReadableOnly<IGroup_Default>
        {
        }

        public interface IGroup_Optional : IBase, IDReadableOnly<IGroup_Optional>
        {
        }

        public interface IGroup_None : IBase, IDFilter<IGroup_None>
        {
        }

        public interface IDFilter<T> where T : IBase
        {
            T OfComponent<TComponent>() where TComponent : struct, IComponent;
        }

        public interface IDWritable<T> where T : IBase
        {
            T OfWritableComponent<TComponent>(out ReadWriteComponentAccessor<TComponent> componentAccessor) where TComponent : struct, IComponent;
            T OfWritableSingltonComponent<TComponent>(out ReadWriteSingletonComponentAccessor<TComponent> componenAccessor) where TComponent : struct, IComponent;
        }

        public interface IDReadableOnly<T> where T : IBase
        {
            T OfReadableOnlyComponent<TComponent>(out ReadOnlyComponentAccessor<TComponent> componentAccessor) where TComponent : struct, IComponent;
            T OfReadableOnlySingletonComponent<TComponent>(out ReadOnlySingletonComponentAccessor<TComponent> componenAccessor) where TComponent : struct, IComponent;
        }

        public interface IBase : IFinalizer, IBackToBulder
        {
        }

        public interface IBackToBulder
        {
            IEntityQueryBuilder BackToBuilder();
        }

        public interface IFinalizer
        {
            IDisposableSystemEntityQuery ToQuery(in bool requireForUpdate = false);
            IDisposableSystemEntityQuery ToQueryAndRebuild(in bool requireForUpdate = false);
        }
    }
}
