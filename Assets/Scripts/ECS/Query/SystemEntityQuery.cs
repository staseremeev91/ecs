using System;
using System.Collections.Generic;

namespace ECS
{
    public interface ISystemQueryHandler
    {
        void RegisterQuery(in IDisposableSystemEntityQuery query, in bool requireForUpdate);
    }

    public interface ISystemEntityQuery : IEntityQueryDefinition, IRebuildableEntityQuery
    {
        IReadOnlyCollection<int> Optional { get; }
        IReadOnlyCollection<int> GetEntities();
        bool IsOptionEnabled(in int componentTypeHash, in QueryComponentOptions option);
        int Count();
        bool IsEmpty();
    }

    public interface IDisposableSystemEntityQuery : ISystemEntityQuery, IDisposable
    {
    }

    public sealed class SystemEntityQuery : IDisposableSystemEntityQuery
    {
        readonly int m_WorldUID;
        readonly HashSet<int> m_OptionalComponenents;
        readonly EntityQuery m_InternalQuery;
        readonly Dictionary<int, QueryComponentOptions> m_ComponentOptions;

        public SystemEntityQuery(in int worldUID, in HashSet<int> includedAnyComponents, in HashSet<int> includedAllComponents, in HashSet<int> excludedComponents, in HashSet<int> optionalComponents, in Dictionary<int, QueryComponentOptions> componentOptions)
        {
            m_WorldUID = worldUID;
            m_OptionalComponenents = optionalComponents;
            m_InternalQuery = EntityQuery.New(includedAnyComponents, includedAllComponents, excludedComponents);
            m_ComponentOptions = componentOptions;
        }

        public IReadOnlyCollection<int> Any =>
            m_InternalQuery.Any;

        public IReadOnlyCollection<int> All =>
            m_InternalQuery.All;

        public IReadOnlyCollection<int> Exclude =>
            m_InternalQuery.Exclude;

        public IReadOnlyCollection<int> Optional =>
            m_OptionalComponenents;

        public bool IsEmpty() =>
            Count() == 0;

        public int Count() =>
            m_InternalQuery.Count(m_WorldUID);

        public void ForceRebuild() =>
            m_InternalQuery.ForceRebuild();

        public IReadOnlyCollection<int> GetEntities() =>
            m_InternalQuery.GetEntities(m_WorldUID);

        public bool IsOptionEnabled(in int componentTypeHash, in QueryComponentOptions option) =>
            (m_ComponentOptions[componentTypeHash] & option) != 0;

        public void Dispose()
        {
            m_OptionalComponenents.Clear();
            m_ComponentOptions.Clear();
            m_InternalQuery.Dispose();
        }
    }
}
