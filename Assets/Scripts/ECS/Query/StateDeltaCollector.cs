﻿using ECS.Core;
using System;
using System.Collections.Generic;

namespace ECS
{
    public sealed class StateDeltaCollector : IDisposable
    {
        readonly List<ICommand> m_CurrentCommands;

        public StateDeltaCollector()
        {
            m_CurrentCommands = new List<ICommand>();

            //SharedComponentMap.Compone += OnComponentStateChanged;
            SharedComponentMap.ComponentsRemovedForWorld += OnComponentsRemovedForWorld;
            SharedComponentMap.ComponentsRemovedForEntity += OnComponentsRemovedForEntity;
        }

        public void Dispose()
        {
            //SharedComponentMap.ComponentStateChanged -= OnComponentStateChanged;
            SharedComponentMap.ComponentsRemovedForWorld -= OnComponentsRemovedForWorld;
            SharedComponentMap.ComponentsRemovedForEntity -= OnComponentsRemovedForEntity;
        }

        //void OnComponentStateChanged(in int world, in int entity, in int componentTypeHash, in SharedComponentMap.ComponentStateChangeType changeType)
        //{
        //    switch(changeType)
        //    {
        //        case SharedComponentMap.ComponentStateChangeType.Added:
        //            OnComponenentAdded(world, entity, componentTypeHash);
        //            break;

        //        case SharedComponentMap.ComponentStateChangeType.Changed:
        //            throw new NotImplementedException();

        //        case SharedComponentMap.ComponentStateChangeType.Removed:
        //            OnComponenentRemoved(world, entity, componentTypeHash);
        //            break;
        //    }
        //}

        void OnComponenentAdded(int world, int entity, int componentTypeHash)
        {
            //if (SharedComponentMap.TryGetDataByHash(world, entity, componentTypeHash, out var component))
            //    m_CurrentCommands.Add(new AddOrSetComponentCommand(world, entity, component));
        }

        void OnComponenentRemoved(in int world, in int entity, in int componentTypeHash) =>
            m_CurrentCommands.Add(new RemoveDataCommand(world, entity, componentTypeHash));

        void OnComponentsRemovedForEntity(in int world, in int entity) =>
            m_CurrentCommands.Add(new RemoveEntityCommand(world, entity));

        void OnComponentsRemovedForWorld(in int world) =>
            m_CurrentCommands.Add(new RemoveWorldEntitiesCommand(world));
    }
}
