﻿using System;
using System.Collections.Generic;

namespace ECS.Core
{
    public sealed class CommandBuffer : ICommandBufferOperator, IDisposable
    {
        static uint s_CurrentBufferUID;

        readonly uint m_UID;
        readonly Queue<CommandBufferOperator.CommandPtr> m_Commands;

        public int CommandsCount
        {
            get
            {
                if (m_Disposed)
                    throw new ObjectDisposedException(GetType().FullName);

                return m_Commands.Count;
            }
        }

        bool m_Disposed;

        public CommandBuffer()
        {
            m_UID = s_CurrentBufferUID++;
            m_Commands = new Queue<CommandBufferOperator.CommandPtr>();
        }

        public void Execute()
        {
            InteranlExecute();
            Clear();
        }

        public void ExecuteAndDispose()
        {
            InteranlExecute();
            Dispose();
        }

        void InteranlExecute()
        {
            if (m_Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            while (m_Commands.Count > 0)
                CommandBufferOperator.Storage.Execute(m_UID, m_Commands.Dequeue());
        }

        public void Schedule<TCommand>(TCommand command) where TCommand : struct, ICommand
        {
            if (m_Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            m_Commands.Enqueue(CommandBufferOperator.Storage.OfType<TCommand>.Push(m_UID, command));
        }

        public void Instantiate(in int world, in int entity) =>
            Instantiate(world, entity, targetWorld: world);

        public void Instantiate(in int world, in int entity, in int targetWorld) =>
            Schedule(new InstantiateCommand(world, entity, targetWorld));

        public void TryAddOrSetComponent<TComponenent>(in int world, in int entity, in TComponenent component) where TComponenent : struct, IComponent =>
            Schedule(new AddOrSetComponentCommand(world, entity, component));

        public void TryAddOrSetSingletonComponent<TComponenent>(in int world, in TComponenent component) where TComponenent : struct, IComponent =>
            Schedule(new AddOrSetComponentCommand(world, SharedComponentMap.FIRST_VALID_ENTITY_ID, component));

        public void TryAddOrSetComponent(in int world, in int entity, in IComponent component) =>
            Schedule(new AddOrSetComponentCommand(world, entity, component));

        public void TryRemoveComponent<TComponenent>(in int world, in int entity) where TComponenent : struct, IComponent =>
            Schedule(new RemoveComponentCommand<TComponenent>(world, entity));

        public void TryRemoveEntity(in int world, in int entity) =>
            Schedule(new RemoveEntityCommand(world, entity));

        public void TryRemoveEntitiesForWorld(in int world) =>
            Schedule(new RemoveWorldEntitiesCommand(world));

        public void SerEnabled(in int world, in int entity, in bool enabled)
        {
            if (enabled)
                TryRemoveComponent<DisabledComponent>(world, entity);
            else
                TryAddOrSetComponent<DisabledComponent>(world, entity, default);
        }

        public void TryAddOrSetBuffer<TBuffer>(in int world, in int entity, in TBuffer buffer) where TBuffer : struct, IBufferComponent
        {
            throw new NotImplementedException();
        }

        public void TryAddOrSetData(in int world, in int entity, in IData data)
        {
            throw new NotImplementedException();
        }

        public void TryAddOrSetData<TData>(in int world, in int entity, in TData data) where TData : struct, IData
        {
            throw new NotImplementedException();
        }

        public void TryRemoveQuery(in int world, in IEntityQuery query)
        {
            foreach (var entity in query.GetEntities(world))
                TryRemoveEntity(world, entity);
        }

        public void TryRemoveQueryAndDispose(in int world, in IDisposableEntityQuery query)
        {
            TryRemoveQuery(world, query);
            query.Dispose();
        }

        public void Clear()
        {
            m_Commands.Clear();
            CommandBufferOperator.Storage.ClearCommandBuffer(m_UID);
        }

        public void Dispose()
        {
            if (m_Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            m_Disposed = true;
            m_Commands.Clear();
            CommandBufferOperator.Storage.DisposeCommandBuffer(m_UID);
        }
    }
}
