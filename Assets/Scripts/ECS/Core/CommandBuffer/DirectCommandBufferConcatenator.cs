﻿namespace ECS.Core
{
    public struct DirectCommandBufferConcatenator : ICommandBufferConcatenator
    {
        void ICommandBufferConcatenator.TryAddOrSetComponent<TComponent>(in int world, in int entity, in TComponent component) =>
            component.WriteToDataLayout(world, entity);

        void ICommandBufferConcatenator.TryAddOrSetSingletonComponent<TComponent>(in int world, in TComponent component) =>
            ComponentMap<TComponent>.Single.AddOrSet(world, component);

        void ICommandBufferConcatenator.TryAddOrSetComponent(in int world, in int entity, in IComponent component) =>
            component.WriteToDataLayout(world, entity);

        void ICommandBufferConcatenator.TryAddOrSetBuffer<TBuffer>(in int world, in int entity, in TBuffer component) =>
            component.WriteToDataLayout(world, entity);

        void ICommandBufferConcatenator.TryAddOrSetData(in int world, in int entity, in IData data) =>
            data.WriteToDataLayout(world, entity);

        void ICommandBufferConcatenator.TryAddOrSetData<TData>(in int world, in int entity, in TData data) =>
            data.WriteToDataLayout(world, entity);
    }
}
