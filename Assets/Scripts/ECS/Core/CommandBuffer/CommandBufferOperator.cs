﻿using System;
using System.Collections.Generic;

namespace ECS.Core
{
    internal static class CommandBufferOperator
    {
        public static class Storage
        {
            readonly static Dictionary<uint, CommandLayoutCollection> m_Commands = new ();

            static void RegisterLayoutForCommand(in uint commandBufferUID, in ILayout layout)
            {
                if (!m_Commands.TryGetValue(commandBufferUID, out var commandBufferLayout))
                    m_Commands.Add(commandBufferUID, commandBufferLayout = new CommandLayoutCollection(commandBufferUID));

                commandBufferLayout.RegisterLayout(layout);
            }

            public static void Execute(in uint commandBufferUID, in CommandPtr commandPtr)
            {
                if (m_Commands.TryGetValue(commandBufferUID, out var layout))
                    layout.Execute(commandPtr);
            }

            public static void ClearCommandBuffer(in uint commandBufferUID)
            {
                if (m_Commands.TryGetValue(commandBufferUID, out var layout))
                    layout.Clear();
            }

            public static void DisposeCommandBuffer(in uint commandBufferUID)
            {
                if (m_Commands.TryGetValue(commandBufferUID, out var layout))
                    layout.Dispose();

                m_Commands.Remove(commandBufferUID);
            }

            public static class OfType<TCommand> where TCommand : struct, ICommand
            {
                readonly static Dictionary<uint, Layout<TCommand>> s_Layout = new();

                public static CommandPtr Push(in uint commandBufferUID, in TCommand command)
                {
                    if (!s_Layout.TryGetValue(commandBufferUID, out var layout))
                    {
                        layout = new Layout<TCommand>(commandBufferUID, s_Layout.Remove);
                        s_Layout.Add(commandBufferUID, layout);

                        RegisterLayoutForCommand(commandBufferUID, layout);
                    }

                    return layout.Push(command);
                }
            }
        }

        public struct CommandPtr
        {
            public readonly int Buffer;
            public readonly int Index;

            public CommandPtr(in int buffer, in int index)
            {
                Buffer = buffer;
                Index = index;
            }
        }

        public interface ILayout : IDisposable
        {
            int TypeHashCode { get; }
            int Position { get; }
            void Execute(in int commandIndex);
            void Clear();
        }

        public sealed class Layout<TCommand> : ILayout where TCommand : struct, ICommand
        {
            int ILayout.TypeHashCode => m_HashCode;
            int ILayout.Position => m_Position;

            readonly uint m_CommandBufferUID;
            readonly int m_HashCode;
            readonly List<TCommand> m_Layout;

            Func<uint, bool> m_LayoutRemover;
            int m_Position;

            public Layout(in uint commandBufferUID, in Func<uint, bool> layoutRemover)
            {
                m_CommandBufferUID = commandBufferUID;
                m_HashCode = TypeLookup.HashCodeOf<TCommand>();
                m_Layout = new List<TCommand>();

                m_LayoutRemover = layoutRemover;
            }

            public CommandPtr Push(in TCommand command)
            {
                m_Position = m_Layout.Count;
                m_Layout.Add(command);

                return new CommandPtr(m_HashCode, m_Position);
            }

            void ILayout.Execute(in int commandIndex) =>
                 m_Layout[commandIndex].Execute();

            void ILayout.Clear() =>
                Clear();

            void IDisposable.Dispose()
            {
                Clear();
                m_LayoutRemover(m_CommandBufferUID);
                m_LayoutRemover = null;
            }

            void Clear()
            {
                m_Position = 0;
                m_Layout.Clear();
            }
        }

        public sealed class CommandLayoutCollection : IDisposable
        {
            public readonly uint UID;
            readonly Dictionary<int, ILayout> m_LayoutByType;

            public CommandLayoutCollection(in uint commandBufferUID)
            {
                UID = commandBufferUID;
                m_LayoutByType = new Dictionary<int, ILayout>();
            }

            public void RegisterLayout(in ILayout layout) =>
                m_LayoutByType[layout.TypeHashCode] = layout;

            public void Execute(in CommandPtr commandPtr)
            {
                if (m_LayoutByType.TryGetValue(commandPtr.Buffer, out var layout))
                    layout.Execute(commandPtr.Index);
            }

            public void Clear()
            {
                foreach (var layout in m_LayoutByType.Values)
                    layout.Clear();
            }

            public void Dispose()
            {
                foreach (var layout in m_LayoutByType.Values)
                    layout.Dispose();

                m_LayoutByType.Clear();
            }
        }
    }
}
