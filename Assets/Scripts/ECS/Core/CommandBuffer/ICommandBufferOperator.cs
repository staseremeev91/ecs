﻿namespace ECS.Core
{
    public interface ICommandBufferOperator : ICommandBufferCommonModificator, ICommandBufferConcatenator
    {
        void TryRemoveComponent<TComponenent>(in int world, in int entity) where TComponenent : struct, IComponent;
        void TryRemoveEntity(in int world, in int entity);
        void TryRemoveEntitiesForWorld(in int world);
        void TryRemoveQuery(in int world, in IEntityQuery query);
        void TryRemoveQueryAndDispose(in int world, in IDisposableEntityQuery query);
        void Instantiate(in int world, in int entity);
        void Instantiate(in int world, in int entity, in int targetWorld);
        void SerEnabled(in int world, in int entity, in bool enabled);
        void Clear();
    }

    public interface ICommandBufferConcatenator
    {
        void TryAddOrSetSingletonComponent<TComponenent>(in int world, in TComponenent component) where TComponenent : struct, IComponent;
        void TryAddOrSetComponent<TComponenent>(in int world, in int entity, in TComponenent component) where TComponenent : struct, IComponent;
        void TryAddOrSetBuffer<TBuffer>(in int world, in int entity, in TBuffer buffer) where TBuffer : struct, IBufferComponent;
        void TryAddOrSetData(in int world, in int entity, in IData data);
        void TryAddOrSetComponent(in int world, in int entity, in IComponent component);
        void TryAddOrSetData<TData>(in int world, in int entity, in TData data) where TData : struct, IData;
    }

    public interface ICommandBufferCommonModificator
    {
        void Schedule<TCommand>(TCommand command) where TCommand : struct, ICommand;
    }
}
