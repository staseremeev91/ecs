﻿namespace ECS.Core
{
    public struct AllocateBufferCommand<TBufferElement> : ICommand where TBufferElement : struct, IBufferElement
    {
        readonly Ptr.EntityPtr m_EntityPtr;

        public AllocateBufferCommand(in int world, in int entity, in int initialCapacity = 32) =>
            m_EntityPtr = new Ptr.EntityPtr(world, entity);

        void ICommand.Execute() =>
            Buffer<TBufferElement>.AllocateTo(m_EntityPtr.World, m_EntityPtr.Entity);
    }

    public struct AddOrSetBufferCommand<TBufferElement> : ICommand where TBufferElement : struct, IBufferElement
    {
        readonly Ptr.EntityPtr m_EntityPtr;
        readonly Buffer<TBufferElement> m_Buffer;

        public AddOrSetBufferCommand(in int world, in int entity, in Buffer<TBufferElement> buffer)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_Buffer = buffer;
        }

        void ICommand.Execute() =>
            m_Buffer.WriteToDataLayout(m_EntityPtr.World, m_EntityPtr.Entity);
    }

    public struct AddOrSetBufferCommand : ICommand
    {
        readonly Ptr.EntityPtr m_EntityPtr;
        readonly IBufferComponent m_BufferComponent;

        public AddOrSetBufferCommand(in int world, in int entity, in IBufferComponent buffer)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_BufferComponent = buffer;
        }

        void ICommand.Execute() =>
            m_BufferComponent.WriteToDataLayout(m_EntityPtr.World, m_EntityPtr.Entity);
    }
}
