﻿namespace ECS.Core
{
    public struct AddOrSetComponentCommand<TComponent> : ICommand where TComponent : struct, IComponent
    {
        readonly Ptr.EntityPtr m_EntityPtr;
        readonly TComponent m_Component;

        public AddOrSetComponentCommand(in int world, in int entity, in TComponent component)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_Component = component;
        }

        void ICommand.Execute() =>
            m_Component.WriteToDataLayout(m_EntityPtr.World, m_EntityPtr.Entity);
    }

    public struct AddOrSetComponentCommand : ICommand
    {
        readonly Ptr.EntityPtr m_EntityPtr;
        readonly IComponent m_Component;

        public AddOrSetComponentCommand(in int world, in int entity, in IComponent componenent)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_Component = componenent;
        }

        void ICommand.Execute() =>
            m_Component.WriteToDataLayout(m_EntityPtr.World, m_EntityPtr.Entity);
    }
}
