namespace ECS.Core
{
    public struct DisposeCommand<T> : ICommand where T : struct, System.IDisposable
    {
        readonly T m_Data;

        public DisposeCommand(in T data) =>
            m_Data = data;

        public void Execute() =>
            m_Data.Dispose();
    }
}
