﻿namespace ECS.Core
{
    public struct RemoveEntityCommand : ICommand
    {
        readonly Ptr.EntityPtr m_EntityPtr;

        public RemoveEntityCommand(in int world, in int entity) =>
            m_EntityPtr = new Ptr.EntityPtr(world, entity);

        void ICommand.Execute() =>
            m_EntityPtr.Dispose();
    }
}