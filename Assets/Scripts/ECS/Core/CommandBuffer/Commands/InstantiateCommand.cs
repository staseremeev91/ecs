namespace ECS.Core
{
    public struct InstantiateCommand : ICommand
    {
        readonly Ptr.EntityPtr m_EntityPtr;
        readonly int m_TargetWorld;

        public InstantiateCommand(in int world, in int entity)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_TargetWorld = world;
        }

        public InstantiateCommand(in int world, in int entity, in int targetWorld)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_TargetWorld = targetWorld;
        }

        void ICommand.Execute() =>
            SharedComponentMap.InstantiateEntityToWorld(
                m_EntityPtr.World,
                m_EntityPtr.Entity,
                m_TargetWorld,
                EntityAllocatorComponenent.Allocate(m_TargetWorld));
    }
}
