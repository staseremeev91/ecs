﻿namespace ECS.Core
{
    public struct RemoveBufferCommand<TBufferElement> : ICommand where TBufferElement : struct, IBufferElement
    {
        readonly Ptr.EntityPtr m_EntityPtr;

        public RemoveBufferCommand(in int world, in int entity) =>
            m_EntityPtr = new Ptr.EntityPtr(world, entity);

        void ICommand.Execute() =>
            BufferMap<TBufferElement>.TryRemove(m_EntityPtr.World, m_EntityPtr.Entity);
    }

    public struct RemoveComponentCommand<TComponent> : ICommand where TComponent : struct, IComponent
    {
        readonly Ptr.ComponentPtr<TComponent> m_ComponentPtr;

        public RemoveComponentCommand(in int world, in int entity) =>
            m_ComponentPtr = Ptr.ComponentPtr<TComponent>.New(world, entity);

        void ICommand.Execute() =>
            m_ComponentPtr.Dispose();
    }

    public struct RemoveDataCommand : ICommand
    {
        readonly Ptr.EntityPtr m_EntityPtr;
        readonly int m_ComponentTypeHash;

        public RemoveDataCommand(in int world, in int entity, in int dataTypeHash)
        {
            m_EntityPtr = new Ptr.EntityPtr(world, entity);
            m_ComponentTypeHash = dataTypeHash;
        }

        void ICommand.Execute() =>
            SharedComponentMap.TryRemoveDataType(m_EntityPtr.World, m_EntityPtr.Entity, m_ComponentTypeHash);
    }
}