using System;
using ECS.Core.Experimental.Collections;
using UnityEngine;

namespace ECS.Core
{
    [Serializable]
    public struct EntityChildCollectionComponent : IComponent
    {
        [SerializeField]
        FixedList32<int> m_Childs;

        public static void Link(in int world, in int entity, in int entityToAdd)
        {
            if (entity == entityToAdd)
                throw new InvalidOperationException();

            if (!SharedComponentMap.EntityExists(world, entity))
                throw new InvalidOperationException();

            if (!SharedComponentMap.EntityExists(world, entityToAdd))
                throw new InvalidOperationException();

            ref var component = ref ComponentMap<EntityChildCollectionComponent>.GetByRefSafe(world, entity);
            component.m_Childs.Add(entityToAdd);

        }

        void IData.WriteToDataLayout(in int world, in int entity) =>
            ComponentMap<EntityChildCollectionComponent>.AddOrSet(world, entity, this);
    }
}
