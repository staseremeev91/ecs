using System;
using System.Collections.Generic;
using UnityEngine;

namespace ECS.Core
{
    public static class TypeLookup
    {
        readonly static Dictionary<int, int> s_HashCodeLookup;
        readonly static Dictionary<int, Type> s_TypeLookup;
        readonly static List<Type> s_TypeList;

        public static IReadOnlyList<Type> All => s_TypeList;

        static TypeLookup()
        {
            s_HashCodeLookup = new Dictionary<int, int>();
            s_TypeLookup = new Dictionary<int, Type>();
            s_TypeList = new List<Type>();

            RebuildTypeLookups();
        }

        public static int HashCodeOf<T>() =>
            HashCodeOf(typeof(T));

        public static int HashCodeOf(Type type) =>
            s_HashCodeLookup[type.GetHashCode()];

        public static bool TryGetType(int hashCode, out Type type) =>
            s_TypeLookup.TryGetValue(hashCode, out type);

        static int ProduseHashCode(Type type)
        {
            var declaringTypeInfo = type.DeclaringType == null ? "none_DT" : type.DeclaringType.FullName;
            var typeNameSpace = string.IsNullOrWhiteSpace(type.Namespace) ? "none_NS" : type.Namespace;
            var assemblyName = type.Assembly.FullName.Substring(0, Math.Min(type.Assembly.FullName.Length, 10));

            var fullTypeInfo = $"{type.FullName}{typeNameSpace}{assemblyName}{declaringTypeInfo}";

            unchecked
            {
                var hash = 23;
                for (var i = 0; i < fullTypeInfo.Length; i++)
                    hash = hash * 31 + fullTypeInfo[i].GetHashCode();

                return hash;
            }
        }

        static void RebuildTypeLookups()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                if (Attribute.IsDefined(assembly, typeof(TypeLookupAssemblyAttribute)))
                    foreach (var type in assembly.GetTypes())
                    {
                        try
                        {
                            var produsedHashCode = ProduseHashCode(type);
                            s_HashCodeLookup.Add(type.GetHashCode(), produsedHashCode);
                            s_TypeLookup.Add(produsedHashCode, type);
                            s_TypeList.Add(type);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError($"Exception was thrown while trying to register type [{type.Assembly.FullName}] [{type.FullName}], '{e.Message}'");
                        }
                    }
        }
    }

    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public sealed class TypeLookupAssemblyAttribute : Attribute { }
}