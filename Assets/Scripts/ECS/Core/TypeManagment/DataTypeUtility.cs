﻿using System;
using System.Collections.Generic;

[assembly: ECS.Core.TypeLookupAssembly]

namespace ECS.Core
{
    public struct TypeDefinition
    {
        public readonly Type Type;
        public readonly int HashCode;

        public TypeDefinition(Type type)
        {
            Type = type;
            HashCode = DataTypeUtility.HashCodeOf(type);
        }
    }

    public static class DataTypeUtility
    {
        readonly static Type s_DataInterfaceType;

        readonly static Dictionary<int, int> s_HashCodeLookup;
        readonly static Dictionary<int, Type> s_TypeLookup;
        readonly static Dictionary<int, int> s_SizeLookup;

        public static IReadOnlyDictionary<int, Type> Types => s_TypeLookup;

        static DataTypeUtility()
        {
            s_DataInterfaceType = typeof(IData);

            s_HashCodeLookup = new Dictionary<int, int>();
            s_TypeLookup = new Dictionary<int, Type>();
            s_SizeLookup = new Dictionary<int, int>();

            RebuildTypeLookups();
        }

        public static int SizeOf<TData>() where TData : struct, IData =>
            SizeOf(typeof(TData));

        public static int SizeOf(in Type type) =>
            SizeOf(HashCodeOf(type));

        public static int SizeOf(in int componentTypeHash)
        {
            if (!s_SizeLookup.TryGetValue(componentTypeHash, out var size))
                throw new InvalidOperationException($"Invalid component hash '{componentTypeHash}'. All component types should implement '{s_DataInterfaceType.FullName}'");

            return size;
        }

        public static int HashCodeOf<TData>() where TData : struct, IData =>
            HashCodeOf(typeof(TData));

        public static int HashCodeOf(in Type type)
        {
            if (!type.IsComponentType())
                throw new InvalidOperationException($"'{type.FullName}' doesn't implements '{s_DataInterfaceType.FullName}'");

            var currentCode = type.GetHashCode();
            if (!s_HashCodeLookup.TryGetValue(currentCode, out var stableHashCode))
                s_HashCodeLookup.Add(currentCode, stableHashCode = ProduseStableHashCode(type));

            if (!s_TypeLookup.ContainsKey(stableHashCode))
                s_TypeLookup.Add(stableHashCode, type);

            if (!s_SizeLookup.ContainsKey(stableHashCode))
                s_SizeLookup.Add(stableHashCode, System.Runtime.InteropServices.Marshal.SizeOf(type));

            return stableHashCode;
        }

        public static bool TryGetType(in string fullName, out Type type) =>
            s_TypeLookup.TryGetValue(ProduseHashCode(fullName), out type);

        public static bool TryGetType(in int hashCode, out Type type) =>
            s_TypeLookup.TryGetValue(hashCode, out type);

        static int ProduseStableHashCode(in Type type) => ProduseHashCode(type.FullName);
        static int ProduseHashCode(in string typeFullName) => typeFullName.GetHashCode();

        static void RebuildTypeLookups()
        {
            foreach (var type in TypeLookup.All)
                if (type.IsComponentType())
                {
                    HashCodeOf(type);

                    if (Attribute.GetCustomAttribute(type, typeof(RequireGenericComponenentTypeAttribute)) is RequireGenericComponenentTypeAttribute genericComponenentRequireAttribute)
                    {
                        foreach (var genericComponenentType in genericComponenentRequireAttribute.Types)
                        {
                            if (!genericComponenentType.IsGenericType)
                                throw new InvalidOperationException();

                            if (!genericComponenentType.IsComponentType())
                                throw new InvalidOperationException($"{genericComponenentType.FullName} is not a componenent type");

                            var genericArguments = genericComponenentType.GetGenericArguments();
                            foreach (var genericArgument in genericArguments)
                                if (!genericArgument.IsValueType)
                                    throw new InvalidOperationException();

                            HashCodeOf(genericComponenentType);
                        }
                    }
                }

        }

        public static bool IsComponentType(this Type type) =>
            type.IsValueType && !type.IsGenericTypeDefinition && s_DataInterfaceType.IsAssignableFrom(type) && !type.Equals(s_DataInterfaceType);

        public static IReadOnlyCollection<Type> GetComponenentTypes() => s_TypeLookup.Values;
    }

    [AttributeUsage(AttributeTargets.Struct, AllowMultiple = false)]
    public class RequireGenericComponenentTypeAttribute : Attribute
    {
        public readonly Type[] Types;

        public RequireGenericComponenentTypeAttribute(params Type[] targetTypes)
        {
            Types = targetTypes;
        }
    }
}
