﻿using System.Collections;
using System.Collections.Generic;

namespace ECS.Core
{
    public class DataMapSnapshot : IReadOnlyCollection<KeyValuePair<int, IData>>
    {
        public readonly int World;
        readonly List<KeyValuePair<int, IData>> m_InternalCollection;

        public DataMapSnapshot(int world, IEnumerable<KeyValuePair<int, IData>> collection)
        {
            World = world;
            m_InternalCollection = new List<KeyValuePair<int, IData>>(collection);
        }

        public int Count => m_InternalCollection.Count;

        public IEnumerator<KeyValuePair<int, IData>> GetEnumerator() =>
            m_InternalCollection.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
}
