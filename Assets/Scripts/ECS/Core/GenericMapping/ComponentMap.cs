﻿using System.Collections.Generic;

namespace ECS
{
    public struct ReadOnlySingletonComponentAccessor<TComponent> where TComponent : struct, IComponent
    {
        readonly Core.DataLayout.DataByEntity<TComponent> m_Layout;

        public ReadOnlySingletonComponentAccessor(in Core.DataLayout.DataByEntity<TComponent> layout) =>
            m_Layout = layout;

        public bool TryGet(out TComponent component) =>
            m_Layout.TryGetSingleton(out component);
    }

    public struct ReadWriteSingletonComponentAccessor<TComponent> where TComponent : struct, IComponent
    {
        public readonly ReadOnlySingletonComponentAccessor<TComponent> ReadOnly;

        readonly Core.DataLayout.DataByEntity<TComponent> m_Layout;

        public ReadWriteSingletonComponentAccessor(in Core.DataLayout.DataByEntity<TComponent> layout)
        {
            m_Layout = layout;
            ReadOnly = new(m_Layout);
        }

        public ref TComponent GetByRef() =>
            ref m_Layout.GetSingletonByRef();
    }

    public struct ReadOnlyComponentAccessor<TComponent> where TComponent : struct, IComponent
    {
        public int Count => m_Layout.Count;
        readonly Core.DataLayout.DataByEntity<TComponent> m_Layout;

        public ReadOnlyComponentAccessor(in Core.DataLayout.DataByEntity<TComponent> layout) =>
            m_Layout = layout;

        public bool Exists(in int entity) =>
            m_Layout.Exists(entity);

        public bool TryGet(in int entity, out TComponent component) =>
            m_Layout.TryGet(entity, out component);

        public TComponent Get(in int entity) =>
            m_Layout.Get(entity);
    }

    public struct ReadWriteComponentAccessor<TComponent> where TComponent : struct, IComponent
    {
        public readonly ReadOnlyComponentAccessor<TComponent> ReadOnly;
        public int Count => m_Layout.Count;

        readonly Core.DataLayout.DataByEntity<TComponent> m_Layout;

        public ReadWriteComponentAccessor(in Core.DataLayout.DataByEntity<TComponent> layout)
        {
            m_Layout = layout;
            ReadOnly = new(m_Layout);
        }

        public ref TComponent GetByRef(int entity) =>
            ref m_Layout.GetByRef(entity);
    }
}


namespace ECS.Core
{
    public sealed class ComponentMap<TComponent> : IDataContainer where TComponent : struct, IComponent
    {
        static readonly TypeDefinition s_Type;
        static readonly DataLayout.DataByWorld<TComponent> s_Components;

        TypeDefinition IDataContainer.TypeDefinition => s_Type;

        ComponentMap() { }

        static ComponentMap()
        {
            s_Type = new TypeDefinition(typeof(TComponent));
            s_Components = new DataLayout.DataByWorld<TComponent>();

            SharedComponentMap.RegisterMap(new ComponentMap<TComponent>());
        }

        #region COLLECTION_PROVIDERS
        static DataLayout.DataByEntity<TComponent> GetWorldComponents(in int world) =>
            s_Components.ForWorld(world);

        public static ReadWriteComponentAccessor<TComponent> GetReadWriteAccessor(in int world) =>
            new (GetWorldComponents(world));

        public static ReadWriteSingletonComponentAccessor<TComponent> GetReadWriteSingltonAccessor(in int world) =>
            new (GetWorldComponents(world));
        #endregion

        #region MODIFICATIONS
        public static bool TryAdd(in int world, in int entity, in TComponent component)
        {
            var componentTypeRegistred = SharedComponentMap.TryRegisterDataType(world, entity, s_Type.HashCode); 
            if (componentTypeRegistred)
                s_Components.AddOrSet(world, entity, component);

            return componentTypeRegistred;
        }

        public static void AddOrSet(in int world, in int entity, in TComponent component) 
        {
            SharedComponentMap.TryRegisterDataType(world, entity, s_Type.HashCode);
            s_Components.AddOrSet(world, entity, component);
        }
            
        public static bool TryRemove(in int world, in int entity) =>
            SharedComponentMap.TryRemoveDataType(world, entity, s_Type.HashCode);

        public static ref TComponent GetByRef(int world, int entity)
        {
            var forWorld = GetWorldComponents(world);
            return ref forWorld.GetByRef(entity);
        }

        public static ref TComponent GetByRefSafe(int world, int entity)
        {
            SharedComponentMap.TryRegisterDataType(world, entity, s_Type.HashCode);
            var forWorld = GetWorldComponents(world);
            return ref forWorld.GetByRefSafe(entity);
        }
        #endregion

        public static bool Exists(in int world, in int entity) =>
            TryGet(world, entity, out _);

        public static bool TryGet(in int world, in int entity, out TComponent component) =>
            s_Components.TryGet(world, entity, out component);

        #region INSTANCE
        void IDataContainer.RemoveDataForWorld_Internal(in int world) =>
            s_Components.RemoveForWorld(world);

        void IDataContainer.RemoveDataForEntity_Internal(in int world, in int entity) =>
            s_Components.TryRemove(world, entity);

        void IDataContainer.CopyData_Internal(in int srcWorld, in int srcEntity, in int dstWorld, in int dstEntity)
        {
            if (!s_Components.TryGet(srcWorld, srcEntity, out var component))
                throw new System.InvalidOperationException($"Unable to preform copy, compoent of type '{s_Type.Type.FullName}' not exists on [{nameof(srcWorld)}:{srcWorld}] [{nameof(srcEntity)}:{srcEntity}]");

            AddOrSet(dstWorld, dstEntity, component);
        }

        IEnumerable<KeyValuePair<int, IData>> IDataContainer.DataProvider_Internal(int world)
        {
            foreach (var kvp in GetWorldComponents(world))
                yield return new KeyValuePair<int, IData>(kvp.Key, kvp.Value);
        }

        bool IDataContainer.DataProvider_Internal(in int world, in int entity, out IData data)
        {
            data = default;

            var componentExists = s_Components.TryGet(world, entity, out var actualComponent);
            if (componentExists)
                data = actualComponent;

            return componentExists;
        }
        #endregion

        public static class Single
        {
            public static bool TryGet(int world, out TComponent component) =>
                ComponentMap<TComponent>.TryGet(world, SharedComponentMap.FIRST_VALID_ENTITY_ID, out component);

            public static void AddOrSet(int world, TComponent component) =>
                ComponentMap<TComponent>.AddOrSet(world, SharedComponentMap.FIRST_VALID_ENTITY_ID, component);

            public static ref TComponent GetByRefSafe(in int world) =>
                ref ComponentMap<TComponent>.GetByRefSafe(world, SharedComponentMap.FIRST_VALID_ENTITY_ID);
        }
    }
}