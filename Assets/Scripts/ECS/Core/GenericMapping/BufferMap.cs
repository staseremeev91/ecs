﻿using System.Collections.Generic;


namespace ECS
{
    public struct ReadWriteBufferAccessor<TBufferElement> where TBufferElement : struct, IBufferElement
    {
        public int Count => m_Layout.Count;

        readonly Core.DataLayout.DataByEntity<Buffer<TBufferElement>> m_Layout;

        public ReadWriteBufferAccessor(in Core.DataLayout.DataByEntity<Buffer<TBufferElement>> layout)
        {
            m_Layout = layout;
        }

        public bool Exists(in int entity) =>
            m_Layout.Exists(entity);

        public bool TryGet(in int entity, out Buffer<TBufferElement> readWriteBuffer) =>
            m_Layout.TryGet(entity, out readWriteBuffer);
    }

    public struct ReadOnlyBufferAccessor<TBufferElement> where TBufferElement : struct, IBufferElement
    {
        public int Count => m_Layout.Count;

        readonly Core.DataLayout.DataByEntity<Buffer<TBufferElement>> m_Layout;

        public ReadOnlyBufferAccessor(in Core.DataLayout.DataByEntity<Buffer<TBufferElement>> layout)
        {
            m_Layout = layout;
        }

        public bool Exists(in int entity) =>
            m_Layout.Exists(entity);

        public bool TryGet(in int entity, out Buffer<TBufferElement>.ReadOnly readOnlyBuffer)
        {
            readOnlyBuffer = default;

            var hasBuffer = m_Layout.TryGet(entity, out var readWriteBuffer);
            if (hasBuffer)
                readOnlyBuffer = readWriteBuffer.AsReadOnly();

            return hasBuffer;
        }
            
    }
}

namespace ECS.Core
{
    public sealed class BufferMap<TBufferElement> : IDataContainer where TBufferElement : struct, IBufferElement
    {
        static readonly TypeDefinition s_Type;
        static readonly DataLayout.DataByWorld<Buffer<TBufferElement>> s_Buffers;

        TypeDefinition IDataContainer.TypeDefinition => s_Type;

        BufferMap() { }

        static BufferMap()
        {
            s_Type = new TypeDefinition(typeof(Buffer<TBufferElement>));
            s_Buffers = new DataLayout.DataByWorld<Buffer<TBufferElement>>();

            SharedComponentMap.RegisterMap(new BufferMap<TBufferElement>());
        }

        #region COLLECTION_PROVIDERS
        static DataLayout.DataByEntity<Buffer<TBufferElement>> GetWorldBuffers(in int world) =>
            s_Buffers.ForWorld(world);
        public static ReadWriteBufferAccessor<TBufferElement> GetReadWriteAccessor(in int world) =>
            new(GetWorldBuffers(world));
        #endregion

        #region MODIFICATIONS
        public static bool TryAdd(in int world, in int entity, in Buffer<TBufferElement> buffer)
        {
            var componentTypeRegistred = SharedComponentMap.TryRegisterDataType(world, entity, s_Type.HashCode); 
            if (componentTypeRegistred)
                s_Buffers.AddOrSet(world, entity, buffer);

            return componentTypeRegistred;
        }

        public static void AddOrSet(in int world, in int entity, in Buffer<TBufferElement> buffer) 
        {
            SharedComponentMap.TryRegisterDataType(world, entity, s_Type.HashCode);
            s_Buffers.AddOrSet(world, entity, buffer);
        }
            
        public static bool TryRemove(in int world, in int entity) =>
            SharedComponentMap.TryRemoveDataType(world, entity, s_Type.HashCode);

        #endregion

        public static bool Exists(in int world, in int entity) =>
            TryGet(world, entity, out _);

        public static bool TryGet(in int world, in int entity, out Buffer<TBufferElement> buffer) =>
            s_Buffers.TryGet(world, entity, out buffer);

        #region INSTANCE
        void IDataContainer.RemoveDataForWorld_Internal(in int world) =>
            s_Buffers.RemoveForWorld(world);

        void IDataContainer.RemoveDataForEntity_Internal(in int world, in int entity) =>
            s_Buffers.TryRemove(world, entity);

        void IDataContainer.CopyData_Internal(in int srcWorld, in int srcEntity, in int dstWorld, in int dstEntity)
        {
            if (!s_Buffers.TryGet(srcWorld, srcEntity, out var buffer))
                throw new System.InvalidOperationException($"Unable to preform copy, buffer of type '{s_Type.Type.FullName}' not exists on [{nameof(srcWorld)}:{srcWorld}] [{nameof(srcEntity)}:{srcEntity}]");

            AddOrSet(dstWorld, dstEntity, buffer);
        }

        IEnumerable<KeyValuePair<int, IData>> IDataContainer.DataProvider_Internal(int world)
        {
            foreach (var kvp in GetWorldBuffers(world))
                yield return new KeyValuePair<int, IData>(kvp.Key, kvp.Value);
        }

        bool IDataContainer.DataProvider_Internal(in int world, in int entity, out IData data)
        {
            data = default;

            var componentExists = s_Buffers.TryGet(world, entity, out var actualComponent);
            if (componentExists)
                data = actualComponent;

            return componentExists;
        }
        #endregion
    }
}