﻿using System.Collections;
using System.Collections.Generic;

namespace ECS.Core.DataLayout
{
    public class DataByEntity<TData> : IEnumerable<KeyValuePair<int, TData>> where TData : struct, IData
    {
        readonly DataAccessor<TData> m_Map;

        public int Count => m_Map.Count;

        public DataByEntity() =>
            m_Map = new DataAccessor<TData>();

        public bool TryGet(in int entity, out TData component) =>
            m_Map.TryGetValue(entity, out component);

        public bool TryGetSingleton(out TData component) =>
            m_Map.TryGetValue(SharedComponentMap.FIRST_VALID_ENTITY_ID, out component);

        public bool Exists(in int entity) =>
            m_Map.ContainsKey(entity);

        public void TryAddOrSet(in int entity, in TData component) =>
            m_Map.Set(entity, component);

        public bool TrySet(in int entity, in TData component)
        {
            var containsComponent = m_Map.ContainsKey(entity);
            if (containsComponent)
                m_Map.Set(entity, component);

            return containsComponent;
        }

        public bool Remove(in int entity) =>
            m_Map.Remove(entity);

        public void Clear() =>
            m_Map.Clear();

        public bool TryGetRaw(in int entity, out IData rawData)
        {
            var exists = m_Map.TryGetValue(entity, out var component);
            rawData = component;
            return exists;
        }

        public ref TData GetByRef(int entity) =>
            ref m_Map.GetByRef(entity);

        public ref TData GetByRefSafe(int entity) =>
            ref m_Map.GetByRefSafe(entity);

        public ref TData GetSingletonByRef() =>
           ref m_Map.GetByRef(SharedComponentMap.FIRST_VALID_ENTITY_ID);

        public TData Get(in int entity) =>
            m_Map.GetByRef(entity);

        public IEnumerator<KeyValuePair<int, TData>> GetEnumerator() => m_Map.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => m_Map.GetEnumerator();
    }
}