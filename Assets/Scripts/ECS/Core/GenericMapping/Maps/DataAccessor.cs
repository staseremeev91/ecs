﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ECS.Core.DataLayout
{
	public class DataAccessor<TData> : IEnumerable<KeyValuePair<int, TData>> where TData : struct, IData
	{
		public int Count { get; private set; }

		const int INITIAL_BUCKET_COUNT = 10;
		const int BUCKET_SIZE = 100;

		Bucket[] m_Buckets;

		public DataAccessor() =>
			m_Buckets = new Bucket[INITIAL_BUCKET_COUNT];

		#region MUTATIONS
		public bool Set(int key, TData value)
		{
			var bucketIndex = key / BUCKET_SIZE;
			var bucket = GetBucket(bucketIndex);

			var elementIndex = key - bucketIndex * BUCKET_SIZE;

			ref var element = ref bucket[elementIndex];
			var wasValid = element.IsValid;
			element.Value = value;
			element.IsValid = true;

			if (!wasValid)
				Count++;

			return !wasValid;
		}

		public bool Remove(int key)
		{
			var bucketIndex = key / BUCKET_SIZE;
			var bucket = GetBucket(bucketIndex);

			var elementIndex = key - bucketIndex * BUCKET_SIZE;
			var removed = bucket.Remove(elementIndex);

			if (removed)
				Count--;

			return removed;
		}

		public void Clear()
		{
			m_Buckets = new Bucket[INITIAL_BUCKET_COUNT];
			Count = 0;
		}

		#endregion

		public bool ContainsKey(in int key)
		{
			var bucketIndex = key / BUCKET_SIZE;
			var bucket = GetBucket(bucketIndex);

			return bucket[key - bucketIndex * BUCKET_SIZE].IsValid;
		}

		public bool TryGetValue(in int key, out TData value)
		{
			var bucketIndex = key / BUCKET_SIZE;
			var bucket = GetBucket(bucketIndex);

			var elementIndex = key - bucketIndex * BUCKET_SIZE;
			return bucket.TryGet(elementIndex, out value);
		}

		public ref TData GetByRef(int key)
		{
			var bucketIndex = key / BUCKET_SIZE;
			var bucket = GetBucket(bucketIndex);

			var elementIndex = key - bucketIndex * BUCKET_SIZE;
			return ref bucket.GetByRef(elementIndex);
		}

		public ref TData GetByRefSafe(int key)
		{
			var bucketIndex = key / BUCKET_SIZE;
			var bucket = GetBucket(bucketIndex);

			var elementIndex = key - bucketIndex * BUCKET_SIZE;
			ref var element = ref bucket[elementIndex];

			if(!element.IsValid)
            {
				element.IsValid = true;
				Count++;
			}

			return ref element.Value;
		}

		public IEnumerator<KeyValuePair<int, TData>> GetEnumerator()
        {
			var bucketIndex = 0;
			foreach (var bucket in m_Buckets)
            {
				if(bucket != null)
                {
					foreach (var bucketElement in bucket)
					{
						var key = bucketIndex * BUCKET_SIZE + bucketElement.Key;
						yield return new KeyValuePair<int, TData>(key, bucketElement.Value);
					}
				}

				bucketIndex++;
			}
        }

		IEnumerator IEnumerable.GetEnumerator() =>
			GetEnumerator();

        Bucket GetBucket(in int bucketIndex)
        {
			if (bucketIndex >= m_Buckets.Length)
				Array.Resize(ref m_Buckets, bucketIndex + 1);

			if (m_Buckets[bucketIndex] == null)
				m_Buckets[bucketIndex] = new Bucket(BUCKET_SIZE);

			return m_Buckets[bucketIndex];
		}

        struct Element
		{
			public bool IsValid;
			public TData Value;
		}

		class Bucket : IEnumerable<KeyValuePair<int, TData>>
		{
			readonly Element[] m_Elements;

			public ref Element this[int index] =>
				ref m_Elements[index];

			public Bucket(in int size) =>
				m_Elements = new Element[size];

			public void Set(in int key, TData value) =>
				m_Elements[key] = new Element() { IsValid = true, Value = value };

			public bool TryGet(in int key, out TData value)
            {
				var element = m_Elements[key];
				value = element.IsValid ? element.Value : default;
				return element.IsValid;
			}

			public ref TData GetByRef(int key)
            {
				ref var element = ref m_Elements[key];
				if (!element.IsValid)
					throw new InvalidOperationException();

				return ref element.Value;
			}


			public bool Remove(in int key)
			{
				ref var element = ref m_Elements[key];
				var wasValid = element.IsValid;
				element.IsValid = false;
				element.Value = default;

				return wasValid;
			}

            public IEnumerator<KeyValuePair<int, TData>> GetEnumerator()
            {
				var bucketIndex = 0;
				foreach (var e in m_Elements)
                {
					if (e.IsValid)
						yield return new KeyValuePair<int, TData>(bucketIndex, e.Value);

					bucketIndex++;
				}
			}

			IEnumerator IEnumerable.GetEnumerator() =>
				GetEnumerator();
        }
	}
}
