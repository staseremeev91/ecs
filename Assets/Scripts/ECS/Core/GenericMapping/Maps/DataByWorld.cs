﻿using System.Collections.Generic;

namespace ECS.Core.DataLayout
{
    public class DataByWorld<TData> where TData : struct, IData
    {
        readonly Dictionary<int, DataByEntity<TData>> m_Map;

        public DataByWorld() =>
            m_Map = new Dictionary<int, DataByEntity<TData>>();

        public DataByEntity<TData> ForWorld(in int world) =>
            GetOrCreateWorldMap(world);

        public bool TryGet(in int world, in int entity, out TData component)
        {
            component = default;
            return m_Map.TryGetValue(world, out var componentsByEntity) && componentsByEntity.TryGet(entity, out component);
        }

        public void AddOrSet(in int world, in int entity, TData component)
        {
            var entitiesList = GetOrCreateWorldMap(world);
            entitiesList.TryAddOrSet(entity, component);
        }

        public bool TryRemove(in int world, in int entity) =>
            m_Map.TryGetValue(world, out var componentsByEntity) && componentsByEntity.Remove(entity);

        public void RemoveForWorld(in int world)
        {
            if (!m_Map.TryGetValue(world, out var componentsByEntity))
                return;

            componentsByEntity.Clear();
        }

        public bool TryGetRaw(in int world, in int entity, out IData rawData)
        {
            rawData = default;
            if (!m_Map.TryGetValue(world, out var componentsByEntity))
                return false;

            return componentsByEntity.TryGetRaw(entity, out rawData);
        }

        public ref TData GetByRef(in int world, in int entity)
        {
            if (!m_Map.TryGetValue(world, out var componentsByEntity))
                throw new System.InvalidOperationException();

            return ref componentsByEntity.GetByRef(entity);
        }

        DataByEntity<TData> GetOrCreateWorldMap(in int world)
        {
            if (!m_Map.TryGetValue(world, out var components))
                m_Map.Add(world, components = new DataByEntity<TData>());

            return components;
        }
    }
}