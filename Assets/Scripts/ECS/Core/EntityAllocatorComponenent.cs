﻿namespace ECS.Core
{
    [System.Serializable]
    public struct EntityAllocatorComponenent : IComponent
    {
        public int CurrentUID => NextEntityUID - 1;

        [UnityEngine.SerializeField]
        int m_Counter;
        public int AllocateUID => SharedComponentMap.FIRST_VALID_ENTITY_ID + ++m_Counter;
        public int NextEntityUID => SharedComponentMap.FIRST_VALID_ENTITY_ID + m_Counter + 1;

        void IData.WriteToDataLayout(in int world, in int entity) =>
            ComponentMap<EntityAllocatorComponenent>.AddOrSet(world, entity, this);

        public static bool IsValid(in int entityId) =>
            entityId >= SharedComponentMap.FIRST_VALID_ENTITY_ID;

        public static int Allocate(in int world)
        {
            ref var allocator = ref Get(world);
            var result = allocator.AllocateUID;
            return result;
        }

        public static ref EntityAllocatorComponenent Get(in int world) =>
            ref ComponentMap<EntityAllocatorComponenent>.Single.GetByRefSafe(world);
    }
}
