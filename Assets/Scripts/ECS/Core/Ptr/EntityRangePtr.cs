﻿using UnityEngine;

namespace ECS.Core.Ptr
{
    [System.Serializable]
    public struct EntityRangePtr : IPtr
    {
        public bool IsValid =>
            m_Validator.IsValid;

        [SerializeField]
        EntityPtr m_Validator;
        public EntityPtr Validator => m_Validator;
        public int World => m_Validator.World;

        [SerializeField]
        int m_Start;
        public int Start => m_Start;

        [SerializeField]
        int m_Length;
        public int Length => m_Length;

        public EntityRangePtr(in EntityPtr validator, in int start, in int length)
        {
            m_Validator = validator;
            m_Start = start;
            m_Length = length;
        }

        public EntityPtr this[in int index]
        {
            get
            {
                EnsureValid();
                return new(m_Validator.World, IndexToEntity(index));
            }
        }
           

        public int IndexToEntity(int index)
        {
            EnsureValid();
            return m_Start + index;
        }

        public EntityRangePtr GetSlice(in int position, in int length)
        {
            EnsureValid();
            var startEntity = IndexToEntity(position);
            if (startEntity < m_Start || position + length > m_Length)
                throw new System.InvalidOperationException();

            return new EntityRangePtr(m_Validator, startEntity, length);
        }       

        public bool CheckConsistency()
        {
            EnsureValid();
            for (var entity = Start; entity < Start + Length; entity++)
                if (!SharedComponentMap.EntityExists(m_Validator.World, entity))
                    return false;

            return true;
        }

        public void Dispose()
        {
            EnsureValid();
            for (var entity = Start; entity < Start + Length; entity++)
                SharedComponentMap.RemoveDataForEntity(m_Validator.World, entity);

            m_Validator.Dispose();
        }

        public override string ToString()
        {
            EnsureValid();
            return $"World: {m_Validator.World}, Range: [{m_Start}:{m_Start + m_Length - 1}]";
        }

        void EnsureValid()
        {
            if (!IsValid)
                throw new System.InvalidOperationException();
        }
    }
}
