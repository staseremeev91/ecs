﻿using UnityEngine;

namespace ECS.Core.Ptr
{
    [System.Serializable]
    public struct EntityPtrValidatorComponent : IComponent
    {
        public void WriteToDataLayout(in int world, in int entity) =>
            ComponentMap<EntityPtrValidatorComponent>.AddOrSet(world, entity, this);
    }

    [System.Serializable]
    public struct EntityPtr : IPtr
    {
        public bool IsValid =>
            EntityAllocatorComponenent.IsValid(m_Entity) &&
            SharedComponentMap.EntityExists(m_World, m_Entity);

        [SerializeField]
        int m_World;
        public int World => m_World;

        [SerializeField]
        int m_Entity;
        public int Entity => m_Entity;

        public EntityPtr(in int world, in int entity)
        {
            m_World = world;
            m_Entity = entity;
        }

        public ComponentPtr<TComponenent> ToComponentPtr<TComponenent>() where TComponenent : struct, IComponent =>
            ComponentPtr<TComponenent>.New(this);

        public override string ToString() =>
            $"World: {m_World} Entity: {m_Entity} IsValid: {IsValid}";

        public void Dispose() =>
            SharedComponentMap.RemoveDataForEntity(m_World, m_Entity);
    }
}
