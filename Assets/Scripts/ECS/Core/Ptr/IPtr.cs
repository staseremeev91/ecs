﻿using System;

namespace ECS.Core.Ptr
{
    public interface IPtr : IDisposable
    {
        bool IsValid { get; }
    }
}