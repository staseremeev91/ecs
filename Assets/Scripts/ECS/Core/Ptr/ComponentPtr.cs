﻿namespace ECS.Core.Ptr
{
    public struct ComponentPtr<TComponenent> : IPtr where TComponenent : struct, IComponent
    {
        public bool IsValid =>
            ComponentMap<TComponenent>.Exists(EntityPtr.World, EntityPtr.Entity);

        public readonly EntityPtr EntityPtr;

        ComponentPtr(in EntityPtr entityPtr) =>
            EntityPtr = entityPtr;

        public TComponenent Get()
        {
            if (!TryGet(out var component))
                throw new System.InvalidOperationException();

            return component;
        }

        public ref TComponenent GetByRef() =>
            ref ComponentMap<TComponenent>.GetByRef(EntityPtr.World, EntityPtr.Entity);

        public bool TryGet(out TComponenent component) =>
            ComponentMap<TComponenent>.TryGet(EntityPtr.World, EntityPtr.Entity, out component);

        public void Set(in TComponenent component) =>
            ComponentMap<TComponenent>.AddOrSet(EntityPtr.World, EntityPtr.Entity, component);

        public static ComponentPtr<TComponenent> New(in int world, in int entity) =>
            new(new EntityPtr(world, entity));

        public static ComponentPtr<TComponenent> New(EntityPtr entity) =>
            new(entity);

        public static implicit operator TComponenent(ComponentPtr<TComponenent> componentPtr) =>
            componentPtr.Get();

        public static implicit operator EntityPtr(ComponentPtr<TComponenent> componentPtr) =>
            componentPtr.EntityPtr;

        public override string ToString() =>
            $"{typeof(TComponenent).FullName}: {EntityPtr}";

        public void Dispose() =>
            ComponentMap<TComponenent>.TryRemove(EntityPtr.World, EntityPtr.Entity);
    }
}