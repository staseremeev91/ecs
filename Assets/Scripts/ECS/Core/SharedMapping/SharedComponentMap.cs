﻿using System;
using System.Collections.Generic;

namespace ECS.Core
{
    internal interface IDataContainer
    {
        TypeDefinition TypeDefinition { get; }

        void RemoveDataForWorld_Internal(in int world);
        void RemoveDataForEntity_Internal(in int world, in int entity);
        IEnumerable<KeyValuePair<int, IData>> DataProvider_Internal(int world);
        bool DataProvider_Internal(in int world, in int entity, out IData component);
        void CopyData_Internal(in int srcWorld, in int srcEntity, in int dstWorld, in int dstEntity);
    }

    /// <summary>
    /// Shared interface for each component map;
    /// </summary>
    internal static class SharedComponentMap
    {
        public const int FIRST_VALID_ENTITY_ID = 1;

        public delegate void WorldOpDelegate(in int world);
        public delegate void EntityOpDelegate(in int world, in int entity);
        public delegate void ComponentRemovedDelegate(in int world, in int entity, in int componentTypeHash);
        public delegate void ComponentAddedDelegate(in int world, in int entity, in int componentTypeHash, in bool newEntityAllocated);

        public static event WorldOpDelegate ComponentsRemovedForWorld = delegate { };
        public static event EntityOpDelegate ComponentsRemovedForEntity = delegate { };

        public static event ComponentAddedDelegate ComponentAdded = delegate { };
        public static event ComponentRemovedDelegate ComponentRemoved = delegate { };

        public static event WorldOpDelegate WorldRestoredFromSnapshot = delegate { };

        public static IReadOnlyCollection<int> Worlds => s_Entities.Worlds;

        static readonly Dictionary<int, IDataContainer> s_TrackedContainers;
        static readonly DataLayout.SharedComponentsByWorld s_Entities;

        static SharedComponentMap()
        {
            s_Entities = new DataLayout.SharedComponentsByWorld();
            s_TrackedContainers = new Dictionary<int, IDataContainer>();
        }

        public static void RegisterMap(in IDataContainer map) => 
            s_TrackedContainers.Add(map.TypeDefinition.HashCode, map);

        [Obsolete("Make it index - safe")]
        public static IReadOnlyDictionary<int, HashSet<int>> GetWorldEntitiesCollection(in int world) => 
            s_Entities[world].Entities;

        #region MODIFICATIONS
        public static void RemoveDataForWorld(in int world)
        {
            foreach (var map in s_TrackedContainers.Values)
                map.RemoveDataForWorld_Internal(world);

            if (s_Entities.TryRemoveDataForWorld(world))
                ComponentsRemovedForWorld(world);
        }

        public static void RemoveDataForEntity(in int world, in int entity)
        {
            foreach (var map in s_TrackedContainers.Values)
                map.RemoveDataForEntity_Internal(world, entity);

            if (s_Entities.TryRemoveDataForEntity(world, entity))
                ComponentsRemovedForEntity(world, entity);
        }

        public static bool TryRemoveDataType(in int world, in int entity, in int typeHash)
        {
            if (s_TrackedContainers.TryGetValue(typeHash, out var map))
                map.RemoveDataForEntity_Internal(world, entity);

            var typeRemoved = s_Entities.TryRemoveDataType(world, entity, typeHash);
            if (typeRemoved)
            {
                if (EntityExists(world, entity))
                    ComponentRemoved(world, entity, typeHash);
                else
                    ComponentsRemovedForEntity(world, entity);
            }
                
            return typeRemoved;
        }

        public static bool TryRegisterDataType(in int world, in int entity, in int typeHash)
        {
            var typeRegistred = s_Entities.TryRegisterDataType(world, entity, typeHash, out var newEntityAllocated);
            if (typeRegistred)
                ComponentAdded(world, entity, typeHash, newEntityAllocated);

            return typeRegistred;
        }

        public static void InstantiateEntityToWorld(in int srcWorld, in int srcEntity, in int dstWorld, in int dstEntity)
        {
            if (!TryGetDataTypeHashes(srcWorld, srcEntity, out var dataTypesHashes))
                throw new InvalidOperationException();

            foreach (var dataTypeHash in dataTypesHashes)
                if (s_TrackedContainers.TryGetValue(dataTypeHash, out var dataContainer))
                    dataContainer.CopyData_Internal(srcWorld, srcEntity, dstWorld, dstEntity);
        }
        #endregion

        public static IEnumerable<KeyValuePair<int, IData>> ComponentsForWorld(int world)
        {
            foreach (var map in s_TrackedContainers.Values)
                foreach (var c in map.DataProvider_Internal(world))
                    yield return c;
        }

        public static DataMapSnapshot BuildSnapshotForWorld(in int world) => 
            new(world, ComponentsForWorld(world));

        public static void RestoreFromSnapshot(in DataMapSnapshot snapshot)
        {
            RemoveDataForWorld(snapshot.World);
            foreach (var kvp in snapshot)
                kvp.Value.WriteToDataLayout(snapshot.World, kvp.Key);

            WorldRestoredFromSnapshot(snapshot.World);
        }

        public static bool TryGetDataTypeHashes(in int world, in int entity, out HashSet<int> componentTypeHashes) =>
            s_Entities.TryGetDataTypeHashes(world, entity, out componentTypeHashes);

        public static bool EntityExists(in int world, in int entity) =>
            s_Entities.EntityExists(world, entity);

        public static bool TryGetData<TData>(in int world, in int entity, out IData component) where TData : struct, IData =>
            TryGetData(world, entity, typeof(TData), out component);

        public static bool TryGetData(in int world, in int entity, in Type componentType, out IData component) => 
            TryGetDataByHash(world, entity, DataTypeUtility.HashCodeOf(componentType), out component);

        public static bool TryGetDataByHash(in int world, in int entity, in int componentTypeHash, out IData component)
        {
            component = default;
            return s_TrackedContainers.TryGetValue(componentTypeHash, out var componentMap) && componentMap.DataProvider_Internal(world, entity, out component);
        }

        public static bool TryGetDatas(in int world, in int entity, out IReadOnlyList<IData> components)
        {
            var result = new List<IData>();
            var hasComponents = TryGetDataTypeHashes(world, entity, out var componentTypeHashes);

            if (hasComponents)
            {
                foreach (var componentTypeHash in componentTypeHashes)
                    if (TryGetDataByHash(world, entity, componentTypeHash, out var component))
                        result.Add(component);
            }

            components = result;
            return hasComponents;
        }

        public static IData GetOrProduseComponent<TData>(in int world, in int entity) where TData : struct, IData =>
            TryGetData<TData>(world, entity, out var compoenent) ? compoenent : ProduseComponennt<TData>();

        public static bool CheckCollision(in int world, in int entity, HashSet<int> colliectionToCheck) =>
            TryGetDataTypeHashes(world, entity, out var components) && components.Overlaps(colliectionToCheck);

        public static bool CheckSubsetCollision(in int world, in int entity, HashSet<int> colliectionToCheck)
        {
            if (!TryGetDataTypeHashes(world, entity, out var componenetns))
                return false;

            foreach (var element in colliectionToCheck)
                if (!componenetns.Contains(element))
                    return false;

            return true;
        }

        public static IData ProduseComponennt<TData>() where TData : struct, IData =>
            Activator.CreateInstance<TData>();

        public static IData ProduseComponennt(in int typeHash)
        {
            if (!DataTypeUtility.TryGetType(typeHash, out var type))
                throw new InvalidOperationException($"'{typeHash}' is not a valid component type");

            return ProduseComponent(type);
        }

        public static IData ProduseComponent(in Type type)
        {
            if (!type.IsComponentType())
                throw new InvalidOperationException($"'{type.FullName}' is not a component type");

            return (IData)Activator.CreateInstance(type);
        }

        public static Type OfType<TData>() where TData : struct, IData =>
            OfType(typeof(TData));

        public static Type OfType(in Type componentType) =>
            typeof(ComponentMap<>)
                .MakeGenericType(componentType);
    }

#if UNITY_EDITOR
    public static class SharedComponentMap_EditorProxy
    {
        public static bool TryGetDataByHash(in int world, in int entity, in int componentTypeHash, out IData component) =>
            SharedComponentMap.TryGetDataByHash(world, entity, componentTypeHash, out component);

        public static bool EntityExists(in int world, in int entity) =>
            SharedComponentMap.EntityExists(world, entity);

        public static bool TryGetDataTypeHashes(in int world, in int entity, out IEnumerable<int> componentTypeHashes, out int totalEntitySize)
        {
            totalEntitySize = 0;
            var result = SharedComponentMap.TryGetDataTypeHashes(world, entity, out var componentHashSet);

            if(result)
            {
                foreach (var componentTypeHash in componentHashSet)
                    totalEntitySize += DataTypeUtility.SizeOf(componentTypeHash);
            }

            componentTypeHashes = componentHashSet;
            return result;
        }

        public static bool TryGetDataListAsString(in int world, in int entity, out string result)
        {
            var hasComponents = SharedComponentMap.TryGetDatas(world, entity, out var components);
            result = string.Empty;

            if (hasComponents)
            {
                var typeList = new List<string>();
                foreach (var component in components)
                    typeList.Add(component.GetType().FullName);

                foreach (var component in components)
                    UnityEngine.Debug.LogError(component.GetType().FullName);

                result = string.Join(", ", typeList);
            }

            return hasComponents;
        }
    }
#endif
}

