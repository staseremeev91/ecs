﻿using System.Collections.Generic;

namespace ECS.Core.DataLayout
{
    internal sealed class SharedComponentsByWorld
    {
        public IReadOnlyCollection<int> Worlds => m_WorldEntities.Keys;
        readonly Dictionary<int, SharedComponentsByEntity> m_WorldEntities;

        public SharedComponentsByEntity this[in int world] => m_WorldEntities[world];

        public SharedComponentsByWorld()
        {
            m_WorldEntities = new Dictionary<int, SharedComponentsByEntity>();
        }

        public bool EntityExists(in int world, in int entity) =>
            m_WorldEntities.TryGetValue(world, out var enitityCollection) && enitityCollection.EntityExists(entity);

        public bool TryAllocateEntityIfNotExists(in int world, in int entity)
        {
            if (!m_WorldEntities.TryGetValue(world, out var enitityCollection))
                m_WorldEntities.Add(world, enitityCollection = new SharedComponentsByEntity());

            return enitityCollection.TryAllocateEntityIfNotExists(entity);
        }

        public bool TryRegisterDataType(in int world, in int entity, in int typeHash, out bool newEntityAllocated)
        {
            newEntityAllocated = TryAllocateEntityIfNotExists(world, entity);
            return m_WorldEntities[world][entity].Add(typeHash);
        }

        public bool TryRemoveDataType(in int world, in int entity, in int typeHash) =>
            m_WorldEntities.TryGetValue(world, out var enitityCollection) && enitityCollection.TryRemoveComponent(entity, typeHash);

        public bool TryRemoveDataForEntity(in int world, in int entity) =>
            m_WorldEntities.TryGetValue(world, out var enitityCollection) && enitityCollection.TryRemoveComponentsForEntity(entity);

        public bool TryRemoveDataForWorld(in int world) => 
            m_WorldEntities.Remove(world);

        public bool TryGetDataTypeHashes(in int world, in int entity, out HashSet<int> componenentTypes)
        {
            componenentTypes = default;
            return m_WorldEntities.TryGetValue(world, out var enitityCollection) && enitityCollection.Entities.TryGetValue(entity, out componenentTypes);
        }
    }
}
