﻿using ECS;
using UnityEngine;

public sealed class VIewMovementSystem : ExecutionSystem
{
    readonly ISystemEntityQuery m_EntityQuery;
    readonly ReadOnlyComponentAccessor<MoveComponent> m_MovementComponents;
    readonly ReadOnlyComponentAccessor<ViewComponent> m_ViewComponents;

    public VIewMovementSystem(in int worldUID) : base(worldUID)
    {
        m_EntityQuery =
            GetQueryBuilder()
                .All()
                    .OfReadableOnlyComponent(out m_MovementComponents)
                    .OfReadableOnlyComponent(out m_ViewComponents)
                .ToQuery();
    }

    protected override void OnExecuteUpdate(in float deltaTime)
    {
        base.OnExecuteUpdate(deltaTime);

        foreach (var targetEntity in m_EntityQuery.GetEntities())
        {
            var movementComponent = m_MovementComponents.Get(targetEntity);
            var viewComponent = m_ViewComponents.Get(targetEntity);

            if (!ECS.Core.GameObjectByRef.TryGetObject(WorldUID, viewComponent.ObjectReference, out var viewGameObject))
                continue;

            var viewTransform = viewGameObject.transform;

            viewTransform.localPosition = movementComponent.Position;
            //viewTransform.localRotation = Quaternion.LookRotation(movementComponent.Orientation);
        }
    }
}



