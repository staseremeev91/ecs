﻿using ECS;

public sealed class WorldTimeSystem : ExecutionSystem
{
    readonly ReadWriteSingletonComponentAccessor<WorldTimeComponent> m_WorldTimeSingletonAccessor;

    public WorldTimeSystem(in int worldUID) : base(worldUID)
    {
        m_WorldTimeSingletonAccessor = GetSingletonAccessor<WorldTimeComponent>();
    }

    protected override void OnExecuteStart()
    {
        base.OnExecuteStart();
        PreUpdateCommandBuffer.TryAddOrSetSingletonComponent(WorldUID, default(WorldTimeComponent));
    }

    protected override void OnExecuteUpdate(in float deltaTime)
    {
        base.OnExecuteUpdate(deltaTime);

        var localDeltaTime = deltaTime;
        ref var timeComponent = ref m_WorldTimeSingletonAccessor.GetByRef();
        timeComponent.Time += localDeltaTime;
        timeComponent.TickCount++;
    }
}