﻿using ECS;

public sealed class VIewInstanceSystem : ExecutionSystem
{
    readonly EntitySetup[] m_EntitySetup;

    readonly ISystemEntityQuery m_EntityQuery;

    readonly ReadOnlyComponentAccessor<BehaviourComponent> m_BehaviourComponents;
    readonly ReadOnlyComponentAccessor<ViewComponent> m_ViewComponents;

    public VIewInstanceSystem(EntitySetup[] setup, in int worldUID) : base(worldUID)
    {
        m_EntitySetup = setup;

        m_EntityQuery =
            GetQueryBuilder()
                .All()
                    .OfReadableOnlyComponent(out m_BehaviourComponents)
                    .BackToBuilder()
                .Optional()
                    .OfReadableOnlyComponent(out m_ViewComponents)
                .ToQuery();
    }

    protected override void OnExecuteUpdate(in float deltaTime)
    {
        base.OnExecuteUpdate(deltaTime);

        foreach (var targetEntity in m_EntityQuery.GetEntities())
        {
            var behaviourComponent = m_BehaviourComponents.Get(targetEntity);
            m_ViewComponents.TryGet(targetEntity, out ViewComponent viewComponent);

            if (ECS.Core.GameObjectByRef.Exists(WorldUID, viewComponent.ObjectReference))
                continue;

            viewComponent.ObjectReference = UpdateView(viewComponent, behaviourComponent);

            if (!viewComponent.IsReferenceIdValid)
                continue;

            PostUpdateCommandBuffer.TryAddOrSetComponent(WorldUID, targetEntity, viewComponent);
        }
    }

    int UpdateView(ViewComponent viewComponenent, BehaviourComponent behaviourComponent)
    {
        foreach (var setup in m_EntitySetup)
        {
            if (setup.Team != behaviourComponent.Team)
                continue;

            if (viewComponenent.IsReferenceIdValid)
            {
                ECS.Core.GameObjectByRef.GetOrReproduse(WorldUID, setup.View, viewComponenent.ObjectReference);
                return viewComponenent.ObjectReference;
            }

            return ECS.Core.GameObjectByRef.Produse(WorldUID, setup.View);
        }

        return ECS.Core.GameObjectUtils.InvalidReferenceId;
    }
}

public struct ViewComponent : IComponent
{
    public int ObjectReference;
    public bool IsReferenceIdValid => ECS.Core.GameObjectUtils.IsReferenceIdValid(ObjectReference);
    void IData.WriteToDataLayout(in int world, in int entity) => ECS.Core.ComponentMap<ViewComponent>.AddOrSet(world, entity, this);
}