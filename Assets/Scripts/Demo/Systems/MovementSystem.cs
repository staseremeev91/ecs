﻿using ECS;
using UnityEngine;

public sealed class MovementSystem : ExecutionSystem
{
    readonly ISystemEntityQuery m_Query;
    readonly ISystemEntityQuery m_TestEventEntityQuery;

    readonly ReadOnlyComponentAccessor<BehaviourComponent> m_BehaviourComponents;
    readonly ReadWriteComponentAccessor<MoveComponent> m_MovementComponents;
    readonly ReadWriteComponentAccessor<DestinationComponent> m_DestinationComponents;
    readonly ReadOnlySingletonComponentAccessor<WorldTimeComponent> m_TimeSingletonAccessor;

    readonly ReadOnlyComponentAccessor<TestEventComponent> m_TestEventAccessor;

    public MovementSystem(in int worldUID) : base(worldUID)
    {
        m_TimeSingletonAccessor = GetReadOnlySingletonAccessor<WorldTimeComponent>();
        m_Query =
            GetQueryBuilder()
                .All()
                    .OfReadableOnlyComponent(out m_BehaviourComponents)
                    .OfWritableComponent(out m_MovementComponents)
                    .OfWritableComponent(out m_DestinationComponents)
                .ToQuery();

        m_TestEventEntityQuery = GetEventEntityQuery(out m_TestEventAccessor);
    }

    protected override void OnExecuteUpdate(in float deltaTime)
    {
        base.OnExecuteUpdate(deltaTime);

        m_TimeSingletonAccessor.TryGet(out var timeComponent);        

        foreach (var targetEntity in m_Query.GetEntities())
        {
            var behaviourComponent = m_BehaviourComponents.Get(targetEntity);
            ref var destinationComponent = ref m_DestinationComponents.GetByRef(targetEntity);
            ref var movementComponent = ref m_MovementComponents.GetByRef(targetEntity);

            var directionToDestination = destinationComponent.Destination - movementComponent.Position;
            movementComponent.Orientation = directionToDestination.normalized;
            var desiredMovement = movementComponent.Orientation * deltaTime * behaviourComponent.Speed;
            movementComponent.Position += desiredMovement;

            //Update destination
            if (desiredMovement.sqrMagnitude >= directionToDestination.sqrMagnitude || destinationComponent.NextDestinationUpdateTime < timeComponent.Time)
            {
                destinationComponent.NextDestinationUpdateTime = timeComponent.Time + behaviourComponent.DestinationChangePeriod;
                destinationComponent.Destination = UnityEngine.Random.insideUnitSphere * 30f;
            }
        }
    }
}

[System.Serializable]
public struct TestEventComponent : ECS.Core.IEventComponent
{
    void IData.WriteToDataLayout(in int world, in int entity) => ECS.Core.ComponentMap<TestEventComponent>.AddOrSet(world, entity, this);
}

[System.Serializable]
public struct BehaviourComponent : IComponent
{
    public AgentTeam Team;
    public float Speed;
    public float DestinationChangePeriod;

    void IData.WriteToDataLayout(in int world, in int entity) => ECS.Core.ComponentMap<BehaviourComponent>.AddOrSet(world, entity, this);
}

[System.Serializable]
public struct DestinationComponent : IComponent
{
    public Vector3 Destination;
    public float NextDestinationUpdateTime;

    void IData.WriteToDataLayout(in int world, in int entity) => ECS.Core.ComponentMap<DestinationComponent>.AddOrSet(world, entity, this);
}

[System.Serializable]
public struct MoveComponent : IComponent
{
    public Vector3 Position;
    public Vector3 Orientation;

    void IData.WriteToDataLayout(in int world, in int entity) => ECS.Core.ComponentMap<MoveComponent>.AddOrSet(world, entity, this);
}

public enum AgentTeam
{
    Red, Green, Blue,
}