A simple to use ECS implementation with following features:
- Direct component access based on generic mapping.
- Multiple world support.
- Easy to implement saving/loading systems.
- Entity querying.
- System command buffers.
- Ability to build an entity based collections - a single entity per element.
- Unity integration (Game object references and proxy componet to convert game object to entity).

Building a basic ecs setup:
1. Systems:
All systems should be inherited from BaseSystem:

```
public class MovementSystem : BaseSystem {}
```

Now, we should implement an update method for the system, where the logic will be setuped:

```
protected override void OnExecuteUpdate(in ECSWorld world, in float deltaTime) {}
```

It's enought so far to senup a ECSWorld.

2. World:
To build a world, we should call a ECSWorld constructor with an array of systems, which are the world should operate on.

```
var movementSystem = new MovementSystem();
var someOtherSystem = new SomeOtherSystem();
var world = new ECSWorld(movementSystem, someOtherSystem, ...);
```

or we able to use a MasterSystem;

```
var masterSystem = new MasterSystem(new MovementSystem(), new SomeOtherSystem(), ....);
var world = new ECSWorld(masterSystem);
```

At this point we shold deside, where is world.Update(deltaTime) will be called;
For unity, it's make sence to create some kind of ECSWorldManager to call the update method.

3. Components
All components sould be a stucts, which are implements IComponent interface. 

```
public struct TransformComponent : IComponent
{
	public Vector3 Position;
	public Vector3 Rotation;

	//This is fast component registration interface so far, probably, this should be auto generated;
	void IComponent.Register(in int world, in int entity) => ECS.Core.ComponentMap<TransformComponent>.TryAddOrSet(world, entity, this);
}
```

4. Creating an entity;
To build entity, we could use a EntityLazyBuilder struct;

Lets assume, that world is a ECSWorld instance, which we define before.

```
var builder = EntityLazyBuilder.New(world.UID);
builder += new TransformComponent();
builder += new SomeOtherComponent();
etc.. ;
```

Builder is assembled, now we can create entity in two ways:
- Build an entity immediately:

```
builder.BuildNow();
```

- Build via command buffer:

```
builder.BuildViaCommandBuffer(in int world, in int entity, ICommandBufferConcatenator buffer);
```

This is a bit more low level style, and usually this should be used while building an entity in system update cycle, to avoid state changing, whild iteration it.

5. Iterate over entities;
The simples way to iterate over entities with expected components is using of EntityQuery. Internaly, entity query gets updated every time somthing in state changes.
EntityQuery should be builder once, in the system constructor.
To build is we should use an EntityQueryBuilder which is has 3 options : Any, All, None.

```
public class MovementSystem : BaseSystem
{
	readonly EntityQuery m_EntityQueryTest;

	public MovementSystem() 
	{
		//We search entities which are contains any on this components: TransformComponent or SomeOtherComponent;
		m_EntityQueryTest = 
			new EntityQueryBuilder()
				.Any()
				.Of<TransformComponent>()
				.Of<SomeOtherComponent>()
				.ToQuery();
	}
	
	protected override void OnExecuteUpdate(in ECSWorld world, in float deltaTime) 
	{
		//Get all entity component pairs in world for TransformComponent;
		var transformComponents = world.GetComponentsWithEntity<TransformComponent>();
		
		//Read only variant on the signature above;
		var transformComponentsReadOnly = world.GetComponentsWithEntityReadOnly<TransformComponent>();

		//Iterate over applicable entities;
		foreach (var entity in m_EntityQueryTest.ForWorld(world.UID))
		{
			//Get component;
			transformComponents.TryGetComponent(entity, out var transformComponent)
			
			//Change component;
			transformComponent.Position += Vector3.forward * deltaTime;
			transformComponent.Rotation *= Quaternion.AngleAxis(Vector3.up, 10f *deltaTime); 
			
			//Set component;
			transformComponents.TrySetComponent(entity, transformComponent);
		}
	}
}
```

6. To be continued....



